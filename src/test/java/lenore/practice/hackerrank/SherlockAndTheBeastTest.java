package lenore.practice.hackerrank;

import org.junit.Assert;
import org.junit.Test;

public class SherlockAndTheBeastTest {

	@Test
	public void testProcessNumber() {
		
		Assert.assertTrue("1: -1", "-1".equals(SherlockAndTheBeast.processNumber(1)));
		Assert.assertTrue("3: 555", "555".equals(SherlockAndTheBeast.processNumber(3)));
		Assert.assertTrue("5: 33333 expected, got " + SherlockAndTheBeast.processNumber(5), "33333".equals(SherlockAndTheBeast.processNumber(5)));
		Assert.assertTrue("11: 55555533333 expected, got " + SherlockAndTheBeast.processNumber(11), "55555533333".equals(SherlockAndTheBeast.processNumber(11)));
		Assert.assertTrue("10: 3333333333 expected, got " + SherlockAndTheBeast.processNumber(10), "3333333333".equals(SherlockAndTheBeast.processNumber(10)));
		
	}

}
