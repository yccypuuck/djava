package lenore.practice.geeksforgeeks;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringCompressorTest {

	@Test
	public void testRun() {
		StringCompressor runner = new StringCompressor();
		
		runner.setInput("abc");
		runner.compress();
		assertTrue("abc -> a1b1c1", runner.getInput().toString().equals("a1b1c1"));
		
		runner.setInput("aaaaaaaaaaaaaaaaaaaabcccccd");
		runner.compress();
		assertTrue("aaaaaaaaaaaaaaaaaaaabcccccd -> a20b1c5d1", runner.getInput().toString().equals("a20b1c5d1"));
		
		runner.setInput("c");
		runner.compress();
		assertTrue("c -> c1", runner.getInput().toString().equals("c1"));
		
		runner.setInput("aaabbbccc");
		runner.compress();
		assertTrue("aaabbbccc -> a3b3c3", runner.getInput().toString().equals("a3b3c3"));
		
		runner.setInput("");
		runner.compress();
		assertTrue("'' -> ''", runner.getInput().toString().equals(""));
		
		runner.setInput(null);
		runner.compress();
		assertTrue("null -> ''", runner.getInput().toString().equals(""));
		
	}
	
}
