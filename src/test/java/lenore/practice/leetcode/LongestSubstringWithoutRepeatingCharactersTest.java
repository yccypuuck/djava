package lenore.practice.leetcode;

import static org.junit.Assert.assertEquals;
import lenore.practice.leetcode.LongestSubstringWithoutRepeatingCharacters;

import org.junit.Test;

public class LongestSubstringWithoutRepeatingCharactersTest {

	@Test
	public void testStrings() {
		
		LongestSubstringWithoutRepeatingCharacters runner = new LongestSubstringWithoutRepeatingCharacters();
		assertEquals("abcdaegabc should be 6", 6,runner.lengthOfLongestSubstring("abcdaegabc"));
		assertEquals("abcdabegct should be 7", 7,runner.lengthOfLongestSubstring("abcdabegct"));
		assertEquals("aaabcaaaaa should be 3", 3,runner.lengthOfLongestSubstring("aaabcaaaaa"));
		assertEquals("abcdecgrtt should be 6", 6,runner.lengthOfLongestSubstring("abcdecgrtt"));
		assertEquals("ggububgvfk should be 6", 6,runner.lengthOfLongestSubstring("ggububgvfk"));
	}
	
}
