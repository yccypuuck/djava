package lenore.practice.leetcode;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MedianOfTwoSortedArraysTest {

	@Test
	public void testAll() {
		MedianOfTwoSortedArrays median = new MedianOfTwoSortedArrays();

		assertEquals(2.5, median.findMedianSortedArrays(new int[] {1}, new int [] {2,3,4}),0.5);
		assertEquals(2.5, median.findMedianSortedArrays(new int[] {}, new int [] {2,3}), 0.5);
		assertEquals(4.5, median.findMedianSortedArrays(new int[] {1,4,5,6}, new int [] {2,3,7,8}), 0.5);
	}
	
}
