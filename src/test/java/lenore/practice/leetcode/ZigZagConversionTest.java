package lenore.practice.leetcode;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ZigZagConversionTest {
	ZigZagConversion zig;
	
	@Before
	public void setup(){
		zig = new ZigZagConversion();
	}
	
	@After
	public void tearDown(){
		zig = null;
	}
	
	@Test
	public void testCorrectWork(){
		assertTrue("PAYPALISHIRING [3] should be converted to PAHNAPLSIIGYIR", zig.convert("PAYPALISHIRING", 3).equals("PAHNAPLSIIGYIR"));
		assertTrue("ABC [2] should be converted to ACB", zig.convert("ABC", 2).equals("ACB"));
		assertTrue("Masha [0] should be converted to Masha", zig.convert("Masha", 0).equals("Masha"));
		assertTrue("Masha is cool [2] should be converted to [Msai olah sco]", zig.convert("Masha is cool", 2).equals("Msai olah sco"));
	}
	
	@Test
	public void testEmptyString(){
		System.out.println(zig.convert("", 2));
	}
	
	@Test
	public void testNullString(){
		System.out.println(zig.convert(null, 2));
	}
	
	@Test
	public void testNegativeIndex(){
		assertTrue("PAYPALISHIRING [-3] should be converted to PAYPALISHIRING", zig.convert("PAYPALISHIRING", -3).equals("PAYPALISHIRING"));
	}
}
