package lenore.practice.leetcode;

import org.junit.Assert;
import org.junit.Test;


public class ContainerWithMostWaterTest {
	ContainerWithMostWater tester = new ContainerWithMostWater();
	
	@Test
	public void testCases() {
		Assert.assertEquals("maxArea of {2,3,10,5,7,8,9} should be 36", 36, tester.maxArea(new int [] {2,3,10,5,7,8,9}));
		Assert.assertEquals("maxArea of {3,2,4,1,5,6,1,2,1,2} should be 18", 18, tester.maxArea(new int [] {3,2,4,1,5,6,1,2,1,2}));
	}

	@Test
	public void testTimeLimitExceeded() {
		int [] test = new int[15000];
		for (int i = 0; i < test.length; i++) {
			test[i] = i+1;
		}
		long start = System.currentTimeMillis();
		System.out.println(tester.maxArea(test));
		long stop = System.currentTimeMillis();
		System.out.println("testTimeLimitExceeded algorithm took " + (stop - start) + "ms");
	}
	
	@Test
	public void testTimeLimitExceededBack() {
		int [] test = new int[15000];
		for (int i = 0; i < test.length; i++) {
			test[i] = test.length - i;
		}
		long start = System.currentTimeMillis();
		System.out.println(tester.maxArea(test));
		long stop = System.currentTimeMillis();
		System.out.println("testTimeLimitExceededBack algorithm took " + (stop - start) + "ms");
	}
}
