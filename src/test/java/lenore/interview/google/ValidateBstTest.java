package lenore.interview.google;

import lenore.utils.ds.tree.BST;
import lenore.utils.ds.tree.BinaryTree;
import lenore.utils.ds.tree.TreeNode;

import org.junit.Test;

public class ValidateBstTest {
	@Test
	public void test() {
		BinaryTree myTree = new BinaryTree();
		myTree.generateTree(18);
		myTree.inOrder();

		BST myBst = new BST();
		myBst.generateTree(8);

		BST bst = new BST();
		System.out.println(bst.isValid(myTree.getRoot()));
		myBst.inOrder();
		System.out.println(myBst.isValid());
		TreeNode root = myBst.getRoot();
		System.out.println(root.value);
		root.left.value = root.value;
		System.out.println(bst.isValid(root));
		bst.inOrder(root);
		System.out.println("\n root.left = root.right: " + bst.isValid(root));
		bst.inOrder(root);
	}

	@Test
	public void testMinMax() {
		BST myBst = new BST();
		myBst.generateTree(8);
		myBst.add(Integer.MIN_VALUE);
		myBst.add(Integer.MAX_VALUE);
		myBst.inOrder();
		System.out.println(myBst.isValid());
	}
}
