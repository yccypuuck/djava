package lenore.book.cormen;

import lenore.utils.TestRunner;

public class PolynomialTest {
	
	public static void main(String[] args) {
		String input = "src/test/resources/IntegerArray.txt";
//		String input = "src/test/resources/smallArray.txt";
//		String input = "src/test/resources/QuickSort.txt";
		
		TestRunner tasks = new TestRunner();
		tasks.setInputFile(input);
		
		PolynomialNaive problem = new PolynomialNaive();
		problem.setX(5);
		tasks.add(problem);
		
		PolynomialHornerRule problemHorner = new PolynomialHornerRule();
		problemHorner.setX(5);
		tasks.add(problemHorner);
		
		tasks.run();
	}
	
}
