package lenore.book.cormen;

public class MatrixTest {
	
	public static void main(String [] arg) {
		Integer[][] testA = new Integer[3][3];
		testA[0] = new Integer [] {1,3,5};
		testA[1] = new Integer [] {7,5,5};
		testA[2] = new Integer [] {5,4,2};
		Matrix matrixA = new Matrix(testA);
		System.out.println(matrixA);
		
		Integer[][] testB = new Integer[4][3];
		testB[0] = new Integer [] {6,8,2};
		testB[1] = new Integer [] {4,2,4};
		testB[2] = new Integer [] {3,2,1};
		testB[3] = new Integer [] {3,2,1};
		Matrix matrixB = new Matrix(testB);
		System.out.println(matrixB);
		
		System.out.println(matrixA.mult(matrixB));
	}
}
