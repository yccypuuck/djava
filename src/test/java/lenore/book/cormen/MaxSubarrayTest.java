package lenore.book.cormen;

import lenore.utils.TestRunner;

public class MaxSubarrayTest {
	
	public static void main(String[] args) {
		String input = "4 -2 7 4 -8 -6 2 9 1 0 -4";
//		String input = "-4 -2 -7 -4 -8 -6 -2 -9 -1 -3 -4";
//		String input = "-4 -2 -7 -4 -8 0 -2 -9 -1 -3 -4";
//		String input = "1 2 3 4 5 6 7 8 9 10 11";
//		String input = "0 0 0 0 0 0 0 0 0 0 0";
		
		TestRunner tasks = new TestRunner();
		tasks.setInputString(input);
		
		tasks.add(new MaxSubarrayBruteForce());
		tasks.add(new MaxSubarrayRecursive());
		tasks.add(new MaxSubarrayLinear());
		tasks.add(new MaxSubarrayLinearKadane());
		
//		tasks.run();
		
		String[] test = new String[6];
		test[0] = "4 -2 7 4 -8 -6 2 9 1 0 -4";
		test[1] = "-4 -2 -7 -4 -8 -6 -2 -9 -1 -3 -4";
		test[2] = "-4 -2 -7 -4 -8 0 -2 -9 -1 -3 -4";
		test[3] = "1 2 3 4 5 6 7 8 9 10 11";
		test[4] = "0 0 0 0 0 0 0 0 0 0 0";
		test[5] = "1 -2 10 3";
		
		for (String onetime : test) {
			TestRunner linear = new TestRunner();
			linear.setInputString(onetime);
			linear.add(new MaxSubarrayLinear());
			linear.run();
		}
	}
	
}
