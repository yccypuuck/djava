package lenore.book.cormen;

public class ComplexNumberTest {
	public static void main (String[] args) {
		int [] aData = new int[] {2, 3};
		int [] bData = new int[] {4, 5};
		ComplexNumber aComplex = new ComplexNumber(aData);
		ComplexNumber bComplex = new ComplexNumber(bData);
		
		System.out.println(aComplex.multiply(bComplex));
		
		ComplexNumber testing = new ComplexNumber("   -   2 -+-- 3 + 5i     - 2   i");
		System.out.println(testing.multiply("1 + 4i"));
		
		System.out.println(new ComplexNumber("3 + 2i").multiply("1 + 4i"));
		
	}
}
