package lenore.utils.ds;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StackMinTest {
	StackMin myStack;
	
	@Before
	public void setUp() {
		myStack = new StackMin();
		
	}
	
	@After
	public void tearDown() {
		myStack = null;
	}

	@Test
	public void testMinimumValueAlwayOnTop() {
		
		myStack.push(5);
		myStack.push(5);
		myStack.push(4);
		myStack.push(4);
		myStack.push(3);
		myStack.push(2);
		myStack.push(1);
		myStack.push(0);

		while (myStack.isEmpty()) {
			assertEquals("Minimum is always on top", myStack.getMinimum(), myStack.pop().data);
		}
	}
	
	@Test
	public void testMinimimValueMiddle() {
		myStack.push(3);
		myStack.push(1);
		myStack.push(5);
		
		assertEquals("Minimum should be one", myStack.getMinimum(), 1);
	}
	
}
