package lenore.utils.ds.heap;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;
import java.util.Random;

public class HeapTest {

	@Test
	public void testMinHeap() {
		Random rnd = new Random();
		Comparator<Integer> comparator = new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		};
		MinHeap<Integer> heap = new MinHeap<Integer>(comparator);
		for (int i = 0; i < 20; i++) {
			int randInt = rnd.nextInt(200);
			heap.add(randInt);
		}

		System.out.println("Min" + heap.toString());
		Integer min = heap.pop();
		while (!heap.isEmpty()) {
			Integer temp = heap.pop();
			Assert.assertTrue("next value should be >= than previous", temp >= min);
			min = temp;
		}
	}
	
	@Test
	public void testMaxHeap() {
		Random rnd = new Random();
		Comparator<Integer> comparator = new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		};
		MaxHeap<Integer> heap = new MaxHeap<Integer>(comparator);
		for (int i = 0; i < 20; i++) {
			int randInt = rnd.nextInt(200);
			heap.add(randInt);
		}

		System.out.println("Max" + heap.toString());
		Integer max = heap.pop();
		while (!heap.isEmpty()) {
			Integer temp = heap.pop();
			System.out.println("next value is: " + temp);
			Assert.assertTrue("next value should be <= than previous", temp <= max);
			max = temp;
		}
	}
	

}
