package lenore.utils;

import lenore.utils.ds.LinkList;
import lenore.utils.ds.node.NodeImpl;

public class TestNode {
	public static void main(String[] args) {
		
		LinkList myList = new LinkList();
		LinkList onList = new LinkList();
		myList.generateList(9);
		onList.generateList(5,2);
		myList.printList("myList");
		onList.printList("onList");
		System.out.println("myList empty? " + myList.isEmpty());
		LinkList rsList = myList.addList(onList);
		rsList.printList("rsList");
		
		myList.deleteNodeByData(3);		
		myList.printList("myList");
		
		NodeImpl link = myList.findLastNthNode(3);
		if (link != null)
            System.out.print(link);
		System.out.println();
		
		myList.deleteDup();
		myList.deleteNodeByData(2);

		myList.printList();
		
		myList.corruptList(6);
		link = myList.findCircular();
		if (link != null)
            System.out.print(link);
    }

}
