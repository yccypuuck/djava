package lenore.utils.substring;

import lenore.utils.ProblemRunner;

/**
 * Created by lenore on 9/13/14.
 */
public class PrefixFuntionTest {
    public static void main(String ... args){
        ProblemRunner task = new ProblemRunner(new PrefixFunction());
        task.inputFromFile("src/test/resources/shortString.txt");
        task.run();
    }
}
