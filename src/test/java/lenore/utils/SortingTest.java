package lenore.utils;

import lenore.utils.sort.MergeInsertionSort;

public class SortingTest {

	public static void main(String[] args) {
		String input = "src/test/resources/IntegerArray.txt";
		// String input = "src/test/resources/QuickSort.txt";

		TestRunner tasks = new TestRunner();
		tasks.setInputFile(input);

		// tasks.add(new MergeSort());
		// tasks.add(new InsertionSort());
		// tasks.add(new QuickSort());
		// tasks.add(new BubbleSort());
		// tasks.add(new MergeSortInversions());

		for (int k = 0; k <= 500 / 2; k = k + 500 / 10) {
			MergeInsertionSort sorter = new MergeInsertionSort();
			sorter.setK(k);
			// tasks.add(sorter);
		}

		tasks.run();

	}
}
