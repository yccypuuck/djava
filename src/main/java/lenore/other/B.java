package lenore.other;

public class B extends A{

	static {
		System.out.println("static block in B");
	}
	
	{
		System.out.println("instance block in B");
	}
	
	B() {
		System.out.println("constructor in B");
	}
	
	public static void main(String [] args) {
		B b = new B();
		b.toString();
	}
	
}
