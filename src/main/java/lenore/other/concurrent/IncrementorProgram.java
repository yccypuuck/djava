package lenore.other.concurrent;

public class IncrementorProgram {
	public static int value = 0;
	
	static Incrementor inc;
	
	public static void main(String [] args) {
		inc = new Incrementor();
		System.out.print("value = ");
		
		inc.start();
		
		for (int i = 0; i <=3; i++) {
			try {
				Thread.sleep(i*2*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			inc.changeAction();
		}
		inc.interrupt();
	}
}

class Incrementor extends Thread {
	private volatile boolean isIncrement = true;

	public void changeAction() {
		isIncrement = !isIncrement;
	}

	public void run() {
		
		while (!Thread.interrupted()) {
			
			if (isIncrement) {
				IncrementorProgram.value++;
			} else {
				IncrementorProgram.value--;
			}
			
			System.out.print(IncrementorProgram.value + " ");
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
		}
		
	}
}