package lenore.other.concurrent;

public class TalkConcurrentProgram {

	static SomeThing mThing;
	static AffableThread secondThread;

	public static void main(String[] args) {

		mThing = new SomeThing();
		Thread myThread = new Thread(mThing);

		secondThread = new AffableThread();
		
		secondThread.start();	
		myThread.start();

		System.out.println("Main thread finished");
	}

}

class AffableThread extends Thread {
	@Override
	public void run() 
	{
		System.out.println("Hello from another parallel thread!");
	}
}

class SomeThing implements Runnable {
	public void run() {
		System.out.println("Hello from parallel thread");
	}
}
