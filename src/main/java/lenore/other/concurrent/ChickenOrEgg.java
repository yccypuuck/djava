package lenore.other.concurrent;

public class ChickenOrEgg {

	static EggVoice eggOpinion;

	public static void main(String[] args) {
		eggOpinion = new EggVoice();
		System.out.println("Let's start");

		eggOpinion.start();

		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Chicken!");
		}

		if (eggOpinion.isAlive()) {
			try {
				eggOpinion.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("First was egg!");
		} else {
			System.out.println("First was chicken!");
		}

		System.out.println("Done..");
	}
}

class EggVoice extends Thread {

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Egg!");
		}
	}
}
