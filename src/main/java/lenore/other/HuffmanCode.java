package lenore.other;

import lenore.utils.ds.heap.MinHeap;

import java.util.Comparator;

public class HuffmanCode {
	Pair[] chars;
	String input;
	MinHeap<Pair> heap;

	public HuffmanCode() {
		Pair comparator = new Pair();
		heap = new MinHeap<>(comparator);
		chars = new Pair[128];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = new Pair();
			chars[i].quantity = 0;
			chars[i].c = (char) i;
		}
	}
	public void setInput(String input) {
		this.input = input;
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			chars[(byte) c].quantity++;
		}
	}
	public void print() {
		for (Pair pair : chars) {
			if (pair.quantity > 0) {
				System.out.println(pair.quantity + " " + pair.c);
			}
		}
		
	}
	
	public void printArchive() {
		
	}

	public static void main(String[] args) {
		String input = "implementation classes should provide four \"standard\" constructors as the following";

		HuffmanCode hc = new HuffmanCode();
		hc.setInput(input);
		hc.print();
	}

}

class Pair implements Comparator<Pair> {
	Integer quantity;
	char c;

	public int compare(Pair o1, Pair o2) {
		return (o1.quantity > o2.quantity ? -1 : (o1.quantity.equals(o2.quantity) ? 0 : 1));
	}
}
