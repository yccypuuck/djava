package lenore.other;

public class A {

	static {
		System.out.println("static block in A");
	}
	
	{
		System.out.println("instance block in A");
	}
	
	A () {
		System.out.println("constructor in A");
	}
	
	
}
