package lenore.other;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

public class CsvLoader {
	private String input;
	private HashMap<String, CellProcessor> processorMap;
	
	private CellProcessor[] getProcessors(String [] header) {
		getProcessorsAsMap();
		CellProcessor[] processors = new CellProcessor[header.length];
		for (int i = 0; i < header.length; i++) {
			processors[i] = processorMap.get(header[i]);
		}

		return processors;
	}
	
	private Map<String,CellProcessor> getProcessorsAsMap() {
		processorMap = new HashMap<String, CellProcessor>();
		processorMap.put("customerNo", new ParseDouble());
		processorMap.put("firstName", new NotNull());
		processorMap.put("birthDate", new ParseDate("dd/MM/yyyy"));
		processorMap.put("married", new Optional(new ParseBool()));
		
		return processorMap;
	}
	
	public CsvLoader(String input) {
		this.input = input;
	}
	
	public void load() {
		try {
			readWithCsvMapReader();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void readWithCsvMapReader() throws Exception {
        
        ICsvMapReader mapReader = null;
        try {
                mapReader = new CsvMapReader(new StringReader(input), CsvPreference.STANDARD_PREFERENCE);
                
                // the header columns are used as the keys to the Map
                final String[] header = mapReader.getHeader(true);
                final CellProcessor[] processors = getProcessors(header);
                
                Map<String, Object> customerMap;
                while( (customerMap = mapReader.read(header, processors)) != null ) {
                        System.out.println(String.format("lineNo=%s, rowNo=%s, customerMap=%s", mapReader.getLineNumber(),
                                mapReader.getRowNumber(), customerMap));
                }
                
        }
        finally {
                if( mapReader != null ) {
                        mapReader.close();
                }
        }
	}
}
