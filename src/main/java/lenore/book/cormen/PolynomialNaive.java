package lenore.book.cormen;

import lenore.utils.Problem;

public class PolynomialNaive extends Problem{
	Number[] polynom;
	public PolynomialNaive() {
		x = 1;
	}
	
	public PolynomialNaive(Number[] input ) {
		polynom = input;
		x = 1;
	}
	public PolynomialNaive(Number[] input, int x ) {
		polynom = input;
		this.x = x;
	}
	
	private int x;
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public Number[] getPolynom() {
		return polynom;
	}

	public void setPolynom(Number[] polynom) {
		this.polynom = polynom;
	}
	
	public Number evaluate() {
		int y = 0;
		for (int i = 0; i < polynom.length; i++){
			int p = 1;
			for (int j = 1; j <= i; j++) {
				p = p * x;
			}
			y += polynom[i].intValue() * p;
		}
		return y;
	}

	public void run() {
		Number [] input = readInputArray();
		PolynomialNaive evaluator = new PolynomialNaive(input);
		logger.info(String.format("when x=%d polynom is %s", x, evaluator.evaluate()));
	}

}
