package lenore.book.cormen;

import lenore.utils.Problem;

/**
 * 
 * @author lenore
 * 
 *         Efficient evaluation of polynomials
 * 
 *         Here we start by assigning to p and then successively multiplying by
 *         x and adding the next coefficient. This code requires n
 *         multiplications and n additions (I'm ignoring here the modification
 *         of the loop variable i, as I ignored it in all other algorithms,
 *         where it was implicit in the Python for loop).
 * 
 *         While asymptotically similar to the iterative method, Horner's method
 *         has better constants and thus is faster.
 * 
 *         Curiously, Horner's rule was discovered in the early 19th century,
 *         far before the advent of computers. It's obviously useful for manual
 *         computation of polynomials as well, for the same reason: it requires
 *         less operations.
 *
 */
public class PolynomialHornerRule extends Problem {
	Number[] polynom;
	private int x;

	public PolynomialHornerRule() {
		x = 1;
	}

	public PolynomialHornerRule(Number[] input) {
		polynom = input;
		x = 1;
	}

	public PolynomialHornerRule(Number[] input, int x) {
		polynom = input;
		this.x = x;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void run() {
		Number[] input = readInputArray();
		PolynomialHornerRule evaluator = new PolynomialHornerRule(input);
		logger.info(String.format("when x=%d polynom is %s", x,
				evaluator.evaluate()));
	}

	private Number evaluate() {
		int y = 0;
		for (int i = polynom.length - 1; i >= 0; i--) {
			// a0 + x(a1 + x(a2 + x(a3 + x(a4)))
			y = x * (polynom[i].intValue() + y);
		}
		return y;
	}

}
