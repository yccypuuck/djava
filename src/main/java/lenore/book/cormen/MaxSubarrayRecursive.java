package lenore.book.cormen;

import lenore.utils.Problem;

public class MaxSubarrayRecursive extends Problem{
	Number[] input;
	private int maxSum, left, right;
	
	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}
	
	public int getMaxSum() {
		return maxSum;
	}

	public void setMaxSum(int maxSum) {
		this.maxSum = maxSum;
	}

	public MaxSubarrayRecursive() {
		
	}
	
	public MaxSubarrayRecursive(Number[] input) {
		this.input = input;
		left = 0;
		right = input[input.length - 1].intValue();
		maxSum = Integer.MIN_VALUE;
	}
	
	public void findMaxSubarray() {
		if (input == null || input.length == 0) {
			return;
		}
		findMaxSubarray(0, input.length - 1);
	}
	
	public void findMaxSubarray(int left, int right) {
		if (left == right) {
			if (input[left].intValue() > maxSum) {
				maxSum = input[left].intValue();
				this.left = left;
				this.right = right;
			}
			return;
		}
		int mid = left + (right - left) / 2;
		findMaxSubarray(left, mid);
		findMaxSubarray(mid + 1, right);
		findMaxCrossingSubarray(mid, left, right);
	}
	
	private void findMaxCrossingSubarray(int mid, int left, int right){
		int leftSum = Integer.MIN_VALUE;
		int maxLeft = left;
		int sum = 0;
		for (int i = mid; i >= left; i--) {
			sum += input[i].intValue();
			if (sum > leftSum) {
				leftSum = sum;
				maxLeft = i;
			}
		}
		
		int rightSum = Integer.MIN_VALUE;
		int maxRight = right;
		sum = 0;
		for (int i = mid + 1; i <= right; i++) {
			sum += input[i].intValue();
			if (sum > rightSum) {
				rightSum = sum;
				maxRight = i;
			}
		}
		if (leftSum + rightSum > maxSum) {
			maxSum = leftSum + rightSum;
			this.left = maxLeft;
			this.right = maxRight;
		}
	}

	public void run() {
		Number[] input = readInputArray();
		MaxSubarrayRecursive runner = new MaxSubarrayRecursive(input); 
		runner.findMaxSubarray();
		logger.info(String.format("Max subarray is %d, [%d; %d]", runner.getMaxSum(), runner.getLeft(), runner.getRight()));
	}
}
