package lenore.book.cormen;

import lenore.utils.Problem;

public class MaxSubarrayBruteForce extends Problem{
	Number[] input;
	private int maxSum, left, right;
	
	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}
	
	public MaxSubarrayBruteForce() {
		
	}
	
	public MaxSubarrayBruteForce(Number[] input) {
		this.input = input;
		left = 0;
		right = input[input.length - 1].intValue();
		maxSum = Integer.MIN_VALUE;
	}
	
	public Number findMaxSubarray() {
		for (int i = 0; i < input.length-1; i++) {
			int sum = input[i].intValue();
			if (sum > maxSum) {
				maxSum = sum;
				left = i;
				right = i;
			}
			for (int j = i+1; j < input.length; j++) {
				sum += input[j].intValue();
				if (sum > maxSum) {
					maxSum = sum;
					left = i;
					right = j;
				}
			}
		}
		return maxSum;
	}

	public void run() {
		Number[] input = readInputArray();
		MaxSubarrayBruteForce runner = new MaxSubarrayBruteForce(input); 
		logger.info(String.format("Max subarray is %s, [%s; %s]", runner.findMaxSubarray(), runner.getLeft(), runner.getRight()));
	}
}
