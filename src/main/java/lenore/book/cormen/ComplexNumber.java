package lenore.book.cormen;

public class ComplexNumber {
	int real;
	int imaginary;
	
	
/*	Real: ac - bd
 *	Imaginary: ad + bc
 *	ac - bd + ad - ad -> a(c + d) - d(a + b)
 *  ac + ad - ac + bc -> a(c + d) - c(a - b)
*/
	public ComplexNumber multiply(ComplexNumber number) {
		ComplexNumber result = new ComplexNumber();
		if (number == null) {
			return result;
		}
		int a = real;
		int b = imaginary;
		int c = number.getReal();
		int d = number.getImaginary();
		
		int s1 = c + d;
		int s2 = a + b;
		int s3 = a - b;
		
		int p1 = a * s1;
		int p2 = d * s2;
		int p3 = c * s3;
		
		result.setReal(p1 - p2);
		result.setImaginary(p1 - p3);
		return result;
		
	}

	public ComplexNumber multiply(String input) {
		return this.multiply(new ComplexNumber(input));
	}
	
	public String toString() {
		return real + (imaginary < 0 ? " - " : " + ") + imaginary + "i";
	}
	
	public ComplexNumber() {
	}

	public ComplexNumber(String input) {
		int j = -1;
		int carrier = 1;
		input = input.trim().replaceAll("\\s+", "");
		if (input.charAt(0) == '-') {
			carrier = -1;
		}
		String[] tokens = input.split("[+\\-]");
		
		for (String elem : tokens) {
			j++;
			if (elem == null || elem.equals("")) {
				continue;
			}
			int iPos = elem.indexOf('i');
			if (iPos > 0) {
				imaginary += carrier * Integer.parseInt(elem.substring(0, iPos).trim());
			} else {
				real += carrier * Integer.parseInt(elem.trim());
			}
			j += elem.length();
			if (j < input.length()) {
				carrier = (input.charAt(j) == '-') ? -1 : 1; 
			}
			
		}
	}
	
	public ComplexNumber(int[] input) {
		if (input == null || input.length == 0) {
			return;
		}
		real = input[0];
		if (input.length > 1) {
			imaginary = input[1];
		}
	}
	
	public int getReal() {
		return real;
	}

	public void setReal(int real) {
		this.real = real;
	}

	public int getImaginary() {
		return imaginary;
	}

	public void setImaginary(int imaginary) {
		this.imaginary = imaginary;
	}
	
}
