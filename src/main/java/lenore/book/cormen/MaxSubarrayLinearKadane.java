package lenore.book.cormen;

import lenore.utils.Problem;

public class MaxSubarrayLinearKadane extends Problem{
	Number[] input;
	private int maxSum, left, right;
	
	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}
	
	public int getMaxSum() {
		return maxSum;
	}

	public void setMaxSum(int maxSum) {
		this.maxSum = maxSum;
	}

	public MaxSubarrayLinearKadane() {
		
	}
	
	public MaxSubarrayLinearKadane(Number[] input) {
		this.input = input;
		left = 0;
		right = input[input.length - 1].intValue();
		maxSum = Integer.MIN_VALUE;
	}
	
	public void findMaxSubarray() {
		if (input == null || input.length == 0) {
			return;
		}
         int max_ending_here = 0;
         for (int i = 0; i < input.length; i++) {
              max_ending_here = Math.max(0,max_ending_here + input[i].intValue());
              if (maxSum < max_ending_here) {
            	  maxSum = max_ending_here;
            	  right = i;
              }
         }
         // calculate left
         int sum = maxSum;
         int i = right;
         while (sum > 0) {
        	 sum = sum - input[i].intValue();
        	 i--;
         }
         left = right == 0 ? 0 : i + 1;
	}
	

	public void run() {
		Number[] input = readInputArray();
		MaxSubarrayLinearKadane runner = new MaxSubarrayLinearKadane(input); 
		runner.findMaxSubarray();
		logger.info(String.format("Max subarray is %d, [%d; %d]", runner.getMaxSum(), runner.getLeft(), runner.getRight()));
	}
}
