package lenore.book.cormen;

public class Matrix {
	private Number[][] matrix;
	private int n;

	public Number[][] getMatrix() {
		return matrix;
	}
	
	public int getHeight() {
		return n;
	}
	
	public int getWidth() {
		return matrix[0].length;
	}

	public void setSize(int size) {
		Matrix test = new Matrix(size);
		test = test.add(this);
		this.setMatrix(test.getMatrix());
	}
	
	public void setMatrix(Number[][] matrix) {
		this.matrix = matrix;
		n = matrix.length;
	}

	public int size() {
		return n;
	}

	public Matrix(Number[][] matrix) {
		super();
		this.matrix = matrix;
		n = matrix.length;
	}

	public Matrix(int n) {
		super();
		this.n = n;
		this.matrix = new Number[n][n];
		for (int i = 0; i < n; i++) {
			matrix[i] = new Number[n];
			for (int j = 0; j < n; j++) {
				matrix[i][j] = 0;
			}
		}
	}
	
	public Matrix add(Matrix b) {
		Number[][] bMatrix = b.getMatrix();
		Number[][] result = new Number[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i < b.size() && j < b.size()) {
					result[i][j] = bMatrix[i][j].intValue() + matrix[i][j].intValue();
				} else {
					result[i][j] = 0;
				}
			}
		}
		return new Matrix(result);
	}
	
	public Matrix sub(Matrix b) {
		Number[][] bMatrix = b.getMatrix();
		Number[][] result = new Number[bMatrix.length][bMatrix.length];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				result[i][j] = matrix[i][j].intValue() - bMatrix[i][j].intValue();
			}
		}
		return new Matrix(result);
	}

	//Strassen method of multiplication
	public Matrix mult(Matrix b) {
		if (this.getWidth() != b.getHeight()) {
			System.out.println("The number of columns on matrix A is not the same as the number of rows of matrix B");
			return null;
		}
		int oldSize = this.align(b);
		if (b.size() == 1) {
			Number[][] bMatrix = b.getMatrix();
			Number[][] result = new Number[1][1];
			result[0][0] = bMatrix[0][0].intValue() * matrix[0][0].intValue();
			return new Matrix(result);
		}
		
		Matrix a11 = this.copy(11);
		Matrix a12 = this.copy(12);
		Matrix a21 = this.copy(21);
		Matrix a22 = this.copy(22);
		Matrix b11 = b.copy(11);
		Matrix b12 = b.copy(12);
		Matrix b21 = b.copy(21);
		Matrix b22 = b.copy(22);
		
		Matrix s1 = b12.sub(b22);
		Matrix s2 = a11.add(a12);
		Matrix s3 = a21.add(a22);
		Matrix s4 = b21.sub(b11);
		Matrix s5 = a11.add(a22);
		Matrix s6 = b11.add(b22);
		Matrix s7 = a12.sub(a22);
		Matrix s8 = b21.add(b22);
		Matrix s9 = a11.sub(a21);
		Matrix s10 = b11.add(b12);
		
		Matrix p1 = a11.mult(s1);
		Matrix p2 = s2.mult(b22);
		Matrix p3 = s3.mult(b11);
		Matrix p4 = a22.mult(s4);
		Matrix p5 = s5.mult(s6);
		Matrix p6 = s7.mult(s8);
		Matrix p7 = s9.mult(s10);
		
		Matrix c11 = p5.add(p6.add(p4.sub(p2)));
		Matrix c12 = p1.add(p2);
		Matrix c21 = p3.add(p4);
		Matrix c22 = p5.add(p1.sub(p3.add(p7)));
		
		Matrix result = new Matrix(c11.size()*2);
		result.glue(c11, 11);
		result.glue(c12, 12);
		result.glue(c21, 21);
		result.glue(c22, 22);
		result.setSize(oldSize);
		return result;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(String.format("Matrix %dx%d:\n", n, matrix[0].length));
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				result.append(matrix[i][j]).append("\t");
			}
			result.append("\n");
		}
		return result.toString();
	}
	
	public Matrix copy() {
		return this.copy(11, n);		
	}
	
	private Matrix copy(int position) {
		return this.copy(position, n/2);
	}
		
	private Matrix copy(int position, int n) {
		Number[][] result = new Number[n][n];
		int rowDisplay = position / 10 - 1;
		int colDisplay = position % 10 - 1;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				result[i][j] = matrix[i+n*rowDisplay][j+n*colDisplay].intValue();
			}
		}
		return new Matrix(result);
	}
	
	private int align(Matrix b) {
		int oldSize = Math.min(Math.min(this.getHeight(), this.getWidth()), Math.min(b.getHeight(), b.getWidth()));
		int newSize = Math.max(Math.max(this.getHeight(), this.getWidth()), Math.max(b.getHeight(), b.getWidth()));
		if (newSize > 1 && newSize % 2 > 0 ) {
			newSize++;
		}
		this.setSize(newSize);
		b.setSize(newSize);
		return oldSize;
	}

	private void glue(Matrix b, int bPosition) {
		Number[][] result = b.getMatrix();
		int rowDisplay = bPosition / 10 - 1;
		int colDisplay = bPosition % 10 - 1;
		for (int i = 0; i < b.size(); i++) {
			for (int j = 0; j < b.size(); j++) {
				matrix[i + rowDisplay * b.size()][j + colDisplay * b.size()] = result[i][j].intValue();
			}
		}
	}
}
