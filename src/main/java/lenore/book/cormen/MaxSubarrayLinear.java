package lenore.book.cormen;

import lenore.utils.Problem;

public class MaxSubarrayLinear extends Problem{
	Number[] input;
	private int maxSum, left, right;
	
	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}
	
	public int getMaxSum() {
		return maxSum;
	}

	public void setMaxSum(int maxSum) {
		this.maxSum = maxSum;
	}

	public MaxSubarrayLinear() {
		
	}
	
	public MaxSubarrayLinear(Number[] input) {
		this.input = input;
		left = 0;
		right = input.length - 1;
	}
	
	public void findMaxSubarray() {
		if (input == null || input.length == 0) {
			return;
		}

		int i = 0;
		int sum = input[i].intValue();
		maxSum = sum;

		for(int j = 0; j < input.length; ++j) {
			sum += input[j].intValue();
			
			if (maxSum < sum) {
				maxSum = sum;
				left = i;
				right = j;
			}
			
			if (sum <= 0) {
				// ignoring "first" negative numbers shift i to first non-negative
				sum = 0;
				i = j + 1;
			}
		}
		System.out.println(String.format("Max subarray is %d, [%d; %d]", maxSum, left, right));
	}
	

	public void run() {
		Number[] input = readInputArray();
		MaxSubarrayLinear runner = new MaxSubarrayLinear(input); 
		runner.findMaxSubarray();
		logger.info(String.format("Max subarray is %d, [%d; %d]", runner.getMaxSum(), runner.getLeft(), runner.getRight()));
	}
}
