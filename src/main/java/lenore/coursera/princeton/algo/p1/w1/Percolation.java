package lenore.coursera.princeton.algo.p1.w1;

import java.util.Random;

public class Percolation {
	private int[] grid;
	private int size;

	// create N-by-N grid, with all sites blocked
	// 0 - top, N * N + 1 - bottom
	public Percolation(int N) {
		if (N <= 0) {
			throw new IllegalArgumentException();
		}

		size = N;
		grid = new int[N * N + 1];
		grid[0] = 0;
		for (int i = 1; i < grid.length; i++) {
			grid[i] = -1;
		}
	}

	// recalculate i and j to 1dimension
	private int xyTo1D(int i, int j) {
		return (i - 1) * size + j;
	}

	// open site (row i, column j) if it is not open already
	public void open(int i, int j) {
		checkBounds(i, j);
		int pos = xyTo1D(i, j);
		if (grid[pos] < 0 || grid[pos] >= pos)
			grid[pos] = pos;

		if (i == 1) {
			grid[pos] = 0;
		}
		if (i > 1 && isOpen(i - 1, j)) {
			union(pos, pos - size);
		}
		if (j > 1 && isOpen(i, j - 1)) {
			union(pos, pos - 1);
		}
		if (j < size && isOpen(i, j + 1)) {
			union(pos, pos + 1);
		}

		if (i < size && isOpen(i + 1, j)) {
			union(pos, pos + size);
		}
		System.out.println(":: open :: [" + i + "] [" + j + "]");
		print();

	}

	// change root of p to point to root of q
	private void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		if (grid[i] > j) {
			grid[i] = j;
		} else {
			grid[j] = i;
		}
	}

	// chase parent pointers until reach root
	private int root(int i) {
		int runner = i;
		while (i != grid[i]) {
			i = grid[i];
		}
		while (runner != i) {
			int test = grid[runner];
			if (grid[runner] > i)
				grid[runner] = i;
			runner = test;
		}
		return i;
	}

	// is site (row i, column j) open?
	public boolean isOpen(int i, int j) {
		checkBounds(i, j);
		return grid[xyTo1D(i, j)] >= 0;
	}

	private void checkBounds(int i, int j) {
		if (i < 1 || j < 1 || i > size || j > size) {
			throw new IndexOutOfBoundsException();
		}
	}

	// is site (row i, column j) full?
	public boolean isFull(int i, int j) {
		checkBounds(i, j);
		int xy = xyTo1D(i, j);
		return grid[xy] >= 0 && root(xy) == 0;
	}

	// does the system percolate?
	public boolean percolates() {
		int lastCell = grid.length - 2;
		for (int i = lastCell; i > lastCell - size; i--) {
			if (grid[i] >= 0 && root(i) == 0) {
				return true;
			}
		}
		return false;
	}

	private void print() {
		StringBuffer sb = new StringBuffer();
		System.out.println(grid[0]);
		for (int i = 1; i < grid.length - 1; i++) {
			sb.append(grid[i]).append("\t ");
			if (i % size == 0) {
				System.out.println(sb.toString());
				sb = new StringBuffer();
			}
		}
		System.out.println(grid[grid.length - 1]);
	}

	// test client (optional)
	public static void main(String[] args) {
		int length = 10;
		Percolation p = new Percolation(length);
		int counter = 0;
		Random randomGenerator = new Random();
		while (!p.percolates()) {
			int i = randomGenerator.nextInt(length) + 1;
			int j = randomGenerator.nextInt(length) + 1;

			p.open(i, j);
			counter++;
		}
		System.out.println("test done, opened cells: " + counter);
	}
}
