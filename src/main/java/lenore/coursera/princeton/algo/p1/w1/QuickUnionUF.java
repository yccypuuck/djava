package lenore.coursera.princeton.algo.p1.w1;

public class QuickUnionUF {
	private int[] id;
	
	//set id of each object to itself
	public QuickUnionUF(int N) {
		id = new int[N];
		for (int i = 0; i < id.length; i++) {
			id[i] = i;
		}
	}
	
	//chase parent pointers until reach root
	private int root(int i) {
		while (i != id[i]) {
			i = id[i];
		}
		return i;
	}
	
	//check if p and 1 have the same root 
	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}
	
	//change root of p to point to root of q
	public void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		id[i] = j;
	}
}
