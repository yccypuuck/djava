package lenore.coursera.princeton.algo.p1.w1;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class PercolationStats {
	WeightedQuickUnionUF percolator;

	// perform T independent experiments on an N-by-N grid
	public PercolationStats(int N, int T) {
		if (N <= 0 || T <= 0) {
			throw new IllegalArgumentException();
		}
		percolator = new WeightedQuickUnionUF(N);
	}

	// sample mean of percolation threshold
	public double mean() {
		return 0.0;
	}

	// sample standard deviation of percolation threshold
	public double stddev() {
		return 0.0;
	}

	// low endpoint of 95% confidence interval
	public double confidenceLo() {
		return 0.0;
	}

	// high endpoint of 95% confidence interval
	public double confidenceHi() {
		return 0.0;
	}

	// test client (described below)
	public static void main(String[] args) {
		int n;
		int t;
		if (args.length < 2) {
			n = 200;
			t = 100;
		} else {
			n = Integer.parseInt(args[0]);
			t = Integer.parseInt(args[1]);
		}

		PercolationStats percolationStats = new PercolationStats(n, t);
		System.out.println(percolationStats.toString());
	}
}
