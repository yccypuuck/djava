package lenore.coursera.princeton.algo.p1.w1;

public class QuickUnionWeightedUF {
	private int[] id;
	private int[] sz;
	
	//set id of each object to itself
	public QuickUnionWeightedUF(int N) {
		id = new int[N];
		for (int i = 0; i < id.length; i++) {
			id[i] = i;
			sz[i] = 1;
		}
	}
	
	//chase parent pointers until reach root
	private int root(int i) {
		while (i != id[i]) {
			id[i] = id[id[i]];
			i = id[i];
		}
		return i;
	}
	
	//check if p and 1 have the same root 
	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}
	
	//change root of p to point to root of q
	//link root of smaller tree to root of larger tree, update sz[] array
	public void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		if (sz[i] < sz[j]) {
			id[i] = j; 
			sz[j] += sz[i];
		} else {
			id[j] = i;
			sz[i] += sz[j];
		}
	}
}
