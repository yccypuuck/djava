package lenore.coursera.algo.part1;

import lenore.utils.sort.MergeSort;

/**
 * Created by ryzma01 on 5/22/2014.
 *
 * Expands MergeSort routine to count insertions for coursera task
 *
 */
public class MergeSortInsertions extends MergeSort{
    private double insertionsNo;

    public double getInsertionsNo() {
        return insertionsNo;
    }

    public MergeSortInsertions() { }
    public MergeSortInsertions(Number[] input) {
        setInputNumbers(input);
    }


    @SuppressWarnings("unchecked")
	protected void merge(int low, int middle, int high) {
        // Copy both parts into the helper array
        Number[] helper = new Number[inputNumbers.length];
        System.arraycopy(inputNumbers, 0, helper, 0, inputNumbers.length);
        int i = low;
        int j = middle + 1;
        int k = low;
        // Copy the smallest values from either the left or the right side back
        // to the original array
        while (i <= middle && j <= high) {
            if ( ((Comparable<Number>)helper[j]).compareTo(helper[i]) > 0) {
                inputNumbers[k] = helper[i];
                i++;
            } else {
                inputNumbers[k] = helper[j];
                insertionsNo = insertionsNo + middle - i + 1;
                j++;
            }
            k++;
        }
        // Copy the rest of the left side of the array into the target array
        while (i <= middle) {
            inputNumbers[k] = helper[i];
            k++;
            i++;
        }
    }


}

