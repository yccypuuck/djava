package lenore.coursera.algo.part1;

import lenore.utils.sort.QuickSort;

/**
 * Created by ryzma01 on 5/26/2014.
 * QuickSort
 */
public class QuickSortWays extends QuickSort{
    int pivotCase = 1;
    private double comparisonNo;

    public QuickSortWays() { }
    public QuickSortWays(Number[] input) {
        setInputNumbers(input);
    }


    public Number [] sort(int parameter) {
        pivotCase = parameter;
        comparisonNo = 0;
        sort(0, inputNumbers.length - 1);
        return inputNumbers;
    }

    public double getComparisonNo(){
        return comparisonNo;
    }

	@SuppressWarnings("unchecked")
	protected void sort(int low, int high){
        // two numbers left, sort them and add one comparison
        if (high <= low) {
            return;
        }
        // one number - nothing to do
        if (high - low == 1) {
            if (((Comparable<Number>)inputNumbers[low]).compareTo(inputNumbers[high]) > 0) swap(low,high);
            comparisonNo++;
            return;
        }

        switch (pivotCase) {
            case 2: selectPivotLast(low, high); break;
            case 3: selectPivotMedian(low, high);break;
            default: selectPivotFirst();break;
        }

        // get pivot's index
        int index = partition(low, high);
        // recur  [] < p > []
        sort(low, index - 1);
        sort(index + 1, high);
    }

    protected int partition(int l, int r) {
        comparisonNo += r - l;
        Double pivot = inputNumbers[l].doubleValue();
        // set marker
        int marker = l + 1;
        for (int j = l+1; j <= r; j++) {
            if ((pivot).compareTo(inputNumbers[j].doubleValue()) > 0) {
                // swap and move marker
                swap(marker++, j);
            }
        }
        // swap pivot value with last value that is less than pivot value
        swap(l,--marker);
        return marker;
    }

}
