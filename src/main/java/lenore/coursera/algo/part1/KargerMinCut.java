package lenore.coursera.algo.part1;

import java.util.Collections;
import java.util.List;

/**
 * Created by ryzma01 on 5/29/2014.
 *
 * Algorithms: Design and Analysis, Part 1
 * Programming Question - 3
 *
 * The file contains the adjacency list representation of a simple undirected graph. There are 200 vertices labeled 1 to 200.
 * The first column in the file represents the vertex label, and the particular row (other entries except the first column)
 * tells all the vertices that the vertex is adjacent to. So for example, the 6th row looks like : "6	155	56	52	120	......".
 * This just means that the vertex with label 6 is adjacent to (i.e., shares an edge with) the vertices with labels 155,56,52,120,......,etc
 * Your task is to code up and run the randomized contraction algorithm for the min cut problem and use it on the above
 * graph to compute the min cut. (HINT: Note that you'll have to figure out an implementation of edge contractions.
 * Initially, you might want to do this naively, creating a new graph from the old every time there's an edge contraction.
 * But you should also think about more efficient implementations.) (WARNING: As per the video lectures, please make sure
 * to run the algorithm many times with different random seeds, and remember the smallest cut that you ever find.)
 * Write your numeric answer in the space provided. So e.g., if your answer is 5, just type 5 in the space provided.
 */

public class KargerMinCut extends lenore.utils.ds.AdjacencyListGraph {
    boolean trace = false;

    public int findMinCut() {
        while (allVerticesList.size() > 2) {
            Collections.shuffle(allVerticesList);
            mergeVertices();
        }
        return graph.get(allVerticesList.get(0)).size();
    }

    private Number getVertex(List<Number> list) {
        Collections.shuffle(list);
        return list.get(list.size() - 1);
    }

    private void mergeVertices() {
        Number nodeMerge = getVertex(allVerticesList);
        List<Number> edgeMerge = graph.remove(nodeMerge);
        Number nodeDelete = getVertex(edgeMerge);
        List<Number> edgeDelete = graph.remove(nodeDelete);
        if (trace) {
            printGraph();
            System.out.println("Merging: " + nodeMerge + " : " + nodeDelete);
        }
        allVerticesList.remove(allVerticesList.indexOf(nodeDelete));
        if (trace) {
            System.out.println("edge merge " + nodeMerge + " : " + edgeMerge);
            System.out.println("edge delete " + nodeDelete + " : " + edgeDelete);

            System.out.println("----------- delete duplicates ------------");
        }
        while ((edgeMerge.lastIndexOf(nodeDelete) > -1) || (edgeDelete.lastIndexOf(nodeMerge) > -1)) {
            edgeMerge.remove(nodeDelete);
            edgeDelete.remove(nodeMerge);
        }
        if (trace) {
            System.out.println("edge merge " + nodeMerge + " : " + edgeMerge);
            System.out.println("edge delete " + nodeDelete + " : " + edgeDelete);

            System.out.println("----------- merge nodes ------------");
        }
        while (edgeDelete.size() > 0) {
            edgeMerge.add(edgeDelete.remove(0));
        }
        if (trace) {
            System.out.println("edge merge " + nodeMerge + " : " + edgeMerge);
            System.out.println("edge delete " + nodeDelete + " : " + edgeDelete);
            System.out.println("allVerticesList:" + allVerticesList);
        }
        graph.put(nodeMerge, edgeMerge);

        for (Number i : allVerticesList) {
            List<Number> edge = graph.remove(i);
            while (edge.lastIndexOf(nodeDelete) > -1) {
                edge.remove(nodeDelete);
                edge.add(nodeMerge);
            }
            graph.put(i, edge);
        }
    }

}