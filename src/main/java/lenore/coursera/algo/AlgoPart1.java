package lenore.coursera.algo;

import java.util.Arrays;

import lenore.coursera.algo.Part1Week1;
import lenore.coursera.algo.Part1Week2;
import lenore.coursera.algo.Part1Week3;
import lenore.utils.ProblemRunner;
import lenore.utils.TestRunner;

/**
 * Created by ryzma01 on 7/4/2014.
 *
 * testing for every task in Algorithms Design part I
 *
 */
public class AlgoPart1 {

	public static void main(String... args) {
		String input1 = "src/test/resources/IntegerArray.txt";
		String input2 = "src/test/resources/QuickSort.txt";
		String input3 = "src/test/resources/kargerMinCut.txt";

		TestRunner tasks = new TestRunner();

		Part1Week1 mergeSortInsertions = new Part1Week1();
		mergeSortInsertions.inputFromFile(input1);
		tasks.add(mergeSortInsertions);

		for (int i = 1; i <= 3; i++) {
			Part1Week2 quickSort = new Part1Week2();
			quickSort.setPivotCase(i);
			quickSort.inputFromFile(input2);
			tasks.add(quickSort);
		}

		Part1Week3 kargerMinCut = new Part1Week3();
		kargerMinCut.inputFromFile(input3);
		tasks.add(kargerMinCut);

		tasks.run();

		week3();
	}

	public static void week3() {
		int attempts = 40;
		int[] minCut = new int[attempts];
		for (int i = 0; i < attempts; i++) {
			Part1Week3 part = new Part1Week3();
			ProblemRunner task = new ProblemRunner(part);
			task.inputFromFile("src/test/resources/kargerMinCut.txt");
			task.run();
			minCut[i] = part.minCut;
		}
		Arrays.sort(minCut);
		System.out.println("Out of " + attempts + " iterations the smallest min cut is " + minCut[0]);
	}
}
