package lenore.coursera.algo.tosort;

import lenore.utils.sort.QuickSort;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by ryzma01 on 6/25/2014.
 *
 * The goal of this problem is to implement a variant of the 2-SUM algorithm
 * (covered in the Week 6 lecture on hash table applications).
 * The file contains 1 million integers, both positive and negative
 * (there might be some repetitions!).This is your array of integers,
 * with the ith row of the file specifying the ith entry of the array.
 * Your task is to compute the number of target values t in the interval [-10000,10000] (inclusive)
 * such that there are distinct numbers x,y in the input file that satisfy x+y=t.
 * (NOTE: ensuring distinctness requires a one-line addition to the algorithm from lecture.)
 * Write your numeric answer (an integer between 0 and 20001) in the space provided.
 *
 */
public class TwoSumArray {
    public Long[] arr;
    public HashSet <Long> mySet;
    public int tSum;
    public HashSet<Long> sums = new HashSet<>();

    public TwoSumArray () {
        mySet = new HashSet<>();
        tSum = 0;
        long t = -10000;
        while (t <= 10000) {
            sums.add(t++);
        }
    }


    public int binSearch (Long number) {
        int first, last, middle;
        first  = 0;
        last   = arr.length - 1;
        middle = (first + last)/2;

        while( first <= last )
        {
            if ( number.compareTo(arr[middle]) > 0 )
                first = middle + 1;
            else if ( arr[middle].equals(number))
            {
                return middle;
            }
            else
                last = middle - 1;

            middle = (first + last)/2;
        }
        return -1;
    }


    public void readArray (File file ) {
        BufferedReader br;
        arr = null;
        String line;
        try {
            br = new BufferedReader (new FileReader(file));
            while  ((line = br.readLine()) != null) {
                mySet.add(Long.parseLong(line));
            }
        }
        catch (IOException ioe) {
            System.out.println("Exception" + ioe);
        }
        arr = new Long[mySet.size()];
        int i = 0;
        for (Long key : mySet) {
            arr[i++] = key;
        }
    }

    public void runSort () {
        QuickSort mySorter = new QuickSort(arr);
        arr = (Long[]) mySorter.sort();
    }

    public void calculateSums () {
        for (Long key : arr) {
            if (key > 20000){
                break;
            }
            Long [] arrSum = copySums();
            for (Long t : arrSum) {
                if (!key.equals(t - key) && mySet.contains(t - key)){
                    System.out.println("Number: " + key + " \tsum: " + t + " \tsumnum:" + ++tSum + " \tremaining sums: " + sums.size());
                    sums.remove(t);
                }
            }
        }
    }

    public Long[] copySums() {
        Long [] a = new Long[sums.size()];
        int i = 0;
        for (Long key : sums) {
            a[i++] = key;
        }
        return a;
    }

    public static void main (String ... args) {
        long start, end;
        File file = new File ("resources/2sum.txt");
        //File file = new File ("resources/2sum_54.txt");
        TwoSumArray my2Sum = new TwoSumArray();
        my2Sum.readArray(file);
        my2Sum.runSort();
        my2Sum.calculateSums();
        start = System.nanoTime();




        end = System.nanoTime();
        System.out.println("sum num: " + my2Sum.tSum);
        System.out.println("running time: " + (end - start));

    }

}

