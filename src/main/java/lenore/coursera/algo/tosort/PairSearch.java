package lenore.coursera.algo.tosort;

import lenore.utils.sort.QuickSort;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by ryzma01 on 5/27/2014.
 *
 * The task is from Amazon interview
 *
 * In a given unsorted array of integers find a all pairs that will add up to the provided integer number.
 * Integers can be negative also
 */

//-----------------------------Mashka---------------------------//

class MashaPairs
{
    public static List<Pair> runHash ( Integer[] numbers, int neededSum ) {
        ArrayList<Pair> list = new ArrayList<>();

        Hashtable<Integer, Integer> myMap = new Hashtable<>();

        System.out.println("// print pairs using hash map");
        for (Integer nodeKey : numbers) {
            Integer addUp = myMap.get(neededSum - nodeKey);
            Integer nodeValue = myMap.remove(nodeKey);

            if (addUp != null) {
                for (int j = 1; j <= addUp; j++) {
                    list.add(new Pair((neededSum - nodeKey),nodeKey));
                    //System.out.println(nodeKey + " : " + (neededSum - nodeKey));
                }
            }
            myMap.put(nodeKey, (nodeValue == null) ? 1 : nodeValue + 1);
        }
        return list;
    }

    public static List<Pair> run ( Integer[] numbers, int neededSum ) {
        ArrayList<Pair> list = new ArrayList<>();

        System.out.println("// print pairs using sort");
        QuickSort mySorter = new QuickSort(numbers);
        numbers = (Integer[]) mySorter.sort();
        int left = 0;
        int right = numbers.length - 1;
        while (left < right) {
            int a = numbers[left];
            int b = numbers[right];
            if ((a > 0) && (a+b < neededSum)) return list;
            if (a == neededSum - b) {
				list.add (new Pair(a, b));
                //System.out.println(a + " : " + b);
            }

            if ((numbers[left] + numbers[right]) < neededSum) {
                left++;
            }else if ((numbers[left] + numbers[right-1]) == neededSum) {
                int i = right - 1;
                while (left < i){
                    if ((numbers[left] + numbers[i]) == neededSum) {
                        list.add(new Pair(numbers[left], numbers[i--]));
                    }
                    else break;
                }
                left++;
            }
            else{
                right--;
            }
        }

        return list;

    }
}


///--------------------------------Alik---------------------------//

class Pair {
    private int x;
    private int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public String toString() {
        return "[" + x + ", " + y + "]";
    }

}

public class PairSearch {

    public static void merge (int[] arr, int[] aux, int left, int right) {
        int arr1Left = left;
        int arr1Right = (left+right)/2;
        int arr2Left = 1+(left+right)/2;
        int auxInd = left;

        while (arr1Left <= arr1Right && arr2Left <= right) {
            if (arr[arr1Left] < arr[arr2Left]) {
                aux[auxInd++] = arr[arr1Left++];
            }
            else {
                aux[auxInd++] = arr[arr2Left++];
            }
        }
        while (arr1Left <= arr1Right)
            aux[auxInd++] = arr[arr1Left++];

        while (arr2Left <= right)
            aux[auxInd++] = arr[arr2Left++];

    }
    public static void mergeSort (int[] arr, int[] aux, int left, int right) {
        if (left == right)
            return;

        int mid = (left+right)/2;
        mergeSort(arr, aux, left, mid);
        mergeSort (arr, aux, mid+1, right);
        merge (arr, aux, left, right);

        System.arraycopy(aux, left, arr, left, right + 1 - left);
    }


    public static List<Pair> getPairs(int[] arr, int sum) {
        List <Pair> list = new ArrayList<>();
        mergeSort(arr, new int[arr.length], 0, arr.length - 1);
        int left = 0;
        int right = arr.length-1;
        while (left <right) {
            if (arr[left]+arr[right] < sum)
                left++;
            else if (arr[left]+arr[right] > sum)
                right--;
            else {
                int repeats1 = 1;
                int repeats2 = 1;
                while (arr[left] == arr[++left] && left < right)
                    repeats1++;
                while (arr[right] == arr[--right] && right > left)
                    repeats2++;
                for (int i=0; i<repeats1*repeats2; i++)
                    list.add(new Pair(arr[left-1], arr[right+1]));
            }
        }

        return list;
    }

    public static int[] readArray (File file ) {
        BufferedReader br = null;
        ArrayList<Integer> nums = new ArrayList<>();
        int[] arr;
        String line;

        try  {
            br = new BufferedReader (new FileReader (file));
            while  ((line = br.readLine()) != null) {
                nums.add (Integer.parseInt (line));
            }
        }
        catch (IOException ioe) {
            System.out.println ("Exception" + ioe);
        } finally {
        	if (br != null) {
        		try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
        arr = new int[nums.size()];
        for (int i=0; i<nums.size(); i++) {
            arr[i] = nums.get(i);
        }
        

        return arr;
    }

    public static void main (String ... args) {
        //File file = new File("resources/output.txt");
        File file = new File ("resources/2sum.txt");
        //File file = new File ("resources/QuickSort.txt");
        //File file = new File ("resources/IntegerArray.txt");
        int[] arr = readArray(file);
        int sum = 3;
        long start, end;
        System.out.println("****needed sum**** " + sum);


        start = System.nanoTime();
        List<Pair> list = getPairs(arr, sum);
        end = System.nanoTime();

        System.out.println("\n****Alik****\nrunning time: " + (end - start));


        System.out.println("\n*****Mashka*****");
        arr = readArray(file);

        Integer[] num = new Integer[arr.length];
        for (int i = 0; i < arr.length; i++)
            num[i] = arr[i];

        start = System.nanoTime();
        List<Pair> hashList = MashaPairs.runHash(num, sum);
        end = System.nanoTime();
        System.out.println("running time: " + (end - start));


        // arr = readArray(file);
        start = System.nanoTime();

        List<Pair> sortList = MashaPairs.run(num, sum);
        end = System.nanoTime();
        System.out.println("running time: " + (end - start));

        System.out.println (list);
        System.out.println(hashList);
        System.out.println(sortList);

    }
}