package lenore.coursera.algo.tosort;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ryzma01 on 6/27/2014.
 * I have no idea what is this!
 */
public class Median {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner s = null;
		int[] array = new int[10000];
		try {
			s = new Scanner(new File("resources/Median.txt"));
			int i = 0;
			while (s.hasNextLine()) {
				array[i] = Integer.parseInt(s.nextLine());
				i++;
			}
		} catch (IOException ioe) {
			System.out.println("Exception" + ioe);
		} finally {
			if (s != null) {
				s.close();
			}
		}

		int m;
		long med = 0;
		for (int k = 0; k < array.length; k++) {
			// int k = 2;
			int temp[] = new int[k + 1];
			System.arraycopy(array, 0, temp, 0, k + 1);
			Arrays.sort(temp);
			if (k % 2 == 0) {
				m = k / 2;
			} else {
				m = (k - 1) / 2;
			}

			System.out.println("arrayIndex [" + k + "]: " + array[k]
					+ " , median index: " + m + " , median: " + array[m]);
			med = med + temp[m];
		}
		System.out.println(med % 10000);
	}
}
