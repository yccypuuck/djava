package lenore.coursera.algo.tosort;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class Train {
    
    public static void space (char [] str)
    {
        int spaceCount = 0,j,length;
        for (char aStr : str) {
            if (aStr == ' ') spaceCount++;
        }
        length = str.length;
        j = length + spaceCount*2;
        str[j] = '\0';
        for (int i = length; i >= 0; i--) {
            if (str[i] == ' '){ 
                str[j--] = '%';
                str[j--] = '2';
                str[j--] = '0';
            }
            else str[j--] = str[i];
        }
    }
    
    
    public static boolean isAnagram (char[] str1, char [] str2) {
        int len1 = str1.length;
        int len2 = str2.length;
        if ( len1 != len2 ) return false;
        String str = new String(str2);
        boolean [] angr = new boolean [len1];
        for (int i = 0; i < len1; i++) {
            angr[i] = false;
        }
        for (char aStr1 : str1) {
            int j = 0;
            while (j < len1) {
                j = str.indexOf(aStr1, j);
                if (j < 0) return false;
                if (!angr[j]) {
                    angr[j] = true;
                    break;
                }
                j++;
            }
        }
        return true;
        
    }
    
    public static void removeDuplicates(char [] str){
        if (str == null) return;
        int len = str.length;
        if (len < 2) return;
        
        int tail = 1;
        for (int i = 1; i < len; ++i) {
            int j;
            for (j = 0; j < tail; ++j) {
                if (str[i] == str[j]) break;
            }
            if (j == tail) {
                str[tail] = str[i];
                ++tail;
            }
        }
        str[tail] = 0;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String ttr = "bambula";
        char[] str = ttr.toCharArray();        
        //removeDuplicates(str);
        String btr = "lambabu";
        char[] rtr = btr.toCharArray(); 
        boolean res = isAnagram(str,rtr);
        if (res) System.out.print("Anagram!");
        else System.out.print("Not an anagram!");
    }
    
}
