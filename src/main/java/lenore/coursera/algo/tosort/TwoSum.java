package lenore.coursera.algo.tosort;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;



/**
 * Created by ryzma01 on 6/24/2014.
 *
 * The goal of this problem is to implement a variant of the 2-SUM algorithm
 * (covered in the Week 6 lecture on hash table applications).
 * The file contains 1 million integers, both positive and negative
 * (there might be some repetitions!).This is your array of integers,
 * with the ith row of the file specifying the ith entry of the array.
 * Your task is to compute the number of target values t in the interval [-10000,10000] (inclusive)
 * such that there are distinct numbers x,y in the input file that satisfy x+y=t.
 * (NOTE: ensuring distinctness requires a one-line addition to the algorithm from lecture.)
 * Write your numeric answer (an integer between 0 and 20001) in the space provided.
 *
 */

public class TwoSum {

    public static void main (String ... args) {
        File file = new File("resources/2sum.txt");
        long start, end;
        HashSet<Long> sums = new HashSet<Long>();
        long t = -10000;
        while (t <= 10000) {
            sums.add(t++);
        }

        start = System.nanoTime();
        HashSet<Long> values = new HashSet<Long>();
        BufferedReader br = null;
        try {
            String line;
            long lineNo = 0;
            br = new BufferedReader (new FileReader(file));
            while  ((line = br.readLine()) != null) {
                lineNo++;
                Long currentNum = Long.parseLong(line);
                values.add(currentNum);
                for (Long key : values) {
                    Long sum = currentNum + key;
                    if ((!currentNum.equals(key)) && (sum >= -10000) && (sum <= 10000) && sums.contains(sum)) {
                        sums.remove(sum);
                        System.out.println("line: " + lineNo + "works! " + sum + "| sums: " + (20001 - sums.size()));
                    }
                }
            }
        }
        catch (IOException ioe) {
            System.out.println("Exception" + ioe);
        }
        finally {
        	if (br != null) {
        		try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
        //
        end = System.nanoTime();

        System.out.println("running time: " + (end - start));
        System.out.println("tSum: " + (20001 - sums.size()));
    }


}
