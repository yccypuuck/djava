package lenore.coursera.crypto;

/**
 * Created by ryzma01 on 7/9/2014.
 * Interface for cryptography ciphers
 */
public interface Cipher {
    String encrypt(String msg);
    String decrypt(String msg);
    void setKey(String value);

}
