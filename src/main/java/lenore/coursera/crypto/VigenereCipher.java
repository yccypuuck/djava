package lenore.coursera.crypto;

import java.util.Arrays;

/**
 * Created by ryzma01 on 7/9/2014.
 * Vigenere cipher for KhanAcademy training
 */
public class VigenereCipher implements Cipher {
    private int[] key;

    public void setKey(String value) {
        value = value.toLowerCase();
        System.out.println(value);
        key = new int[value.length()];
        for (int i = 0; i < value.length(); i++) {
            key[i] = value.charAt(i) - 'a';
            System.out.println(key[i]);
        }
        System.out.println(Arrays.toString(key));
    }

    public String encrypt(String msg){
        char [] encrypted = new char [msg.length()];
        int runner = 0;
        for (int i = 0; i < msg.length(); i++) {
            char a = msg.charAt(i);
            //Don't encrypt spaces
            if(a != ' ') {
                //check if we need to go cycle
                char b = (char)(Character.toLowerCase(a) + key[runner]);
                if (b > 'z')
                    a = (char) ((a + key[runner++] - 26));
                else
                    a = (char) ((a + key[runner++]));
                if (runner == key.length)
                    runner = 0;
            }
            encrypted[i] = a;
        }

        return new String(encrypted);
    }


    public String decrypt(String msg){
        char [] encrypted = new char [msg.length()];
        int runner = 0;

        for (int i = 0; i < msg.length(); i++) {
            char a = msg.charAt(i);
            if (a != ' ') {
                char b = (char)(Character.toLowerCase(a) - key[runner]);
                if (b < 'a')
                    a = (char) ((a - key[runner] + 26));
                else
                    a = (char) ((a - key[runner]));
                runner = (runner == key.length - 1) ? 0 : runner + 1;
            }
            encrypted[i] = a;
        }

        return new String(encrypted);    }

    public static void main (String[] args) {
        VigenereCipher myCipher = new VigenereCipher();
        myCipher.setKey("Tiffany");
        String myMsg = myCipher.encrypt("The zebra wears a brown necklace");
        System.out.println(myMsg);
        System.out.println(myCipher.decrypt(myMsg));

    }
}
