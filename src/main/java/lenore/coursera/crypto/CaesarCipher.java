package lenore.coursera.crypto;

public class CaesarCipher {

	public String encrypt(String message, int shift) {
		if (message == null || message.length() == 0 || shift == 0) {
			return message;
		}
		int alphabetLength = 26;
		int startC = 'a';

		message = message.toLowerCase();
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < message.length(); i++) {
			char c = message.charAt(i);
			if (c != ' ' && c >= startC && c <= startC + alphabetLength) {
				int j = c + shift;
				if (j < startC) 
					j += alphabetLength;
				if (j > startC + alphabetLength)
					j -= alphabetLength;
				c = (char) j;
			}
			result.append(c);
		}
		return result.toString();
	}

	public String decrypt(String message, int shift) {
		return encrypt(message, -shift);
	}

	public static void main(String[] args) {
		CaesarCipher cipher = new CaesarCipher();
		System.out.println(cipher.encrypt("abz, cdeZA", 4));
		System.out.println(cipher.decrypt(cipher.encrypt("abz, cdeZA", 4), 4));
        System.out.println(cipher.encrypt("abz, cdeZA", 2));
        System.out.println(cipher.decrypt(cipher.encrypt("abz, cdeZA", 2), 2));
	}

}
