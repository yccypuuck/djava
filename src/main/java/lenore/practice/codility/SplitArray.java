package lenore.practice.codility;
public class SplitArray {
	public int solution(int[] A) {
		// write your code in Java SE 8
		if (A.length > 100000)
			return 0;
		int i = 0;
		int j = A.length - 1;
		int leftSum = 0;
		int rightSum = 0;
		while (i <= j) {
			if (Math.abs(A[i]) > 1000 || Math.abs(A[j]) > 1000)
				return 0;
			if (leftSum < rightSum) {
				leftSum += A[i++];
			} else {
				rightSum += A[j--];
			}
		}

		return Math.abs(leftSum - rightSum);
	}
}
