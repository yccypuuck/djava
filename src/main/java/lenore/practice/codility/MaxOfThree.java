package lenore.practice.codility;

public class MaxOfThree {

	public static void main(String[] args) {
		int a = 5;
		int b = 3;
		int c = 8;
		
		int max1 = (a>b)?a:b;
		int max2 = (a+b-max1 < c)?c:(a+b-max1);
		System.out.println(max1 + " --- " + max2);
	}

}
