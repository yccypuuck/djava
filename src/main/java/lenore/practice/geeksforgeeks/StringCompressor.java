package lenore.practice.geeksforgeeks;

import java.util.Stack;

public class StringCompressor {
	/**
	 * Given “aaabbbccc” it should return “a3b3c3″ in place. Initially I
	 * explained brute force approach then he said array has 2n space
	 */
	private StringBuilder input;
	private int pointer;

	public StringBuilder getInput() {
		return input;
	}

	public void setInput(String input) {
		pointer = 0;
		if (input == null) {
			this.input = new StringBuilder("");
		} else {
			this.input = new StringBuilder(input);
		}
	}

	public void compress() {
		if (input.length() == 0) {
			return;
		}
		int count = 1;
		int current = 1;
		while (current <= input.length()) {
			if (current == input.length() || input.charAt(current) != input.charAt(pointer)) {
				writeCount(count);
				count = 1;
				if (current != input.length()) {
					input.setCharAt(pointer, input.charAt(current));
				}
			} else {
				count++;
			}
			current++;
		}
		input.setLength(pointer);
		writeOnes();
	}

	private void writeCount(int count) {
		int p = ++pointer;
		if (count > 1) {
			Stack<Integer> output = new Stack<>();
			while (count > 0) {
				output.push(count % 10);
				count = count / 10;
			}
			pointer += output.size();
			while (!output.isEmpty()) {
				input.setCharAt(p++, Character.forDigit(output.pop(), 10));
			}
		}
	}

	private void writeOnes() {
		int current = 0;
		while (current < input.length()) {
			if (input.charAt(current++) >= 'a') {
				if (current == input.length() || input.charAt(current) >= 'a') {
					input.insert(current++, '1');
				}
			}
		}
	}
}
