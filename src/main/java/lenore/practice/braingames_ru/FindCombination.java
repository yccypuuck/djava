package lenore.practice.braingames_ru;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindCombination {
	public static float sum;
	String formula = "";
	ArrayList<Float> numbers;
	public static char[] operations = new char[] { '-', '+', '*', '/', '^' };

	public FindCombination() {
	}

	public FindCombination(String formula) {
		this.setFormula(formula);
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Float> getNumbers() {
		return (ArrayList<Float>) numbers.clone();
	}

	public void setNumbers(List<Float> numbers) {
		this.numbers = new ArrayList<>();
		this.numbers.addAll(numbers);
	}

	@SuppressWarnings("unchecked")
	public void processRandom() {
		if (this.numbers == null || this.numbers.size() == 1 && this.numbers.get(0) == sum) {
			System.out.println("you won!");
			System.out.println(formula + " ===> " + this.numbers.get(0));
		} else {
			for (Float firstNum : this.numbers) {
				FindCombination kid;
				ArrayList<Float> gotFirstNumArray = getNumbers();
				gotFirstNumArray.remove(firstNum);
				for (Float secondNum : gotFirstNumArray) {
					ArrayList<Float> gotSecondNumArray;

					for (char c : operations) {
						gotSecondNumArray = (ArrayList<Float>) gotFirstNumArray.clone();
						gotSecondNumArray.remove(secondNum);
						kid = new FindCombination(this.formula + " ===> (" + firstNum + c + secondNum + ")" + gotSecondNumArray);
						switch (c) {
						case '-':
							gotSecondNumArray.add(firstNum - secondNum);
							break;
						case '+':
							gotSecondNumArray.add(firstNum + secondNum);
							break;
						case '*':
							gotSecondNumArray.add(firstNum * secondNum);
							break;
						case '/':
							gotSecondNumArray.add(firstNum / secondNum);
							break;
						case '^':
							gotSecondNumArray.add((float) Math.pow(firstNum, secondNum));
							break;
						default:
						}
						kid.setNumbers(gotSecondNumArray);
						kid.processRandom();
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Float[] testArray = { 1f, 3f, 4f, 6f };
		List<Float> numbers = Arrays.asList(testArray);
		FindCombination runner = new FindCombination();
		FindCombination.operations = new char[] { '-', '+', '*', '/' };
		FindCombination.sum = 24;
		runner.setNumbers(numbers);
		runner.processRandom();
	}

}
