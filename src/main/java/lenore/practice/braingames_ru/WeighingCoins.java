package lenore.practice.braingames_ru;

public class WeighingCoins {
	/**
	 * There are 12 coins, all identical in appearance, and all identical in
	 * weight except for one, which is either heavier or lighter than the
	 * remaining 11 coins. Devise a procedure to identify the counterfeit coin
	 * in only 3 weighings with a balance. Mathematical solution is here:
	 * http://ega-math.narod.ru/Quant/Shestpl.htm
	 * http://www.problems.ru/view_problem_details_new.php?id=60922
	 */
}
