package lenore.practice.hackerrank;

import java.util.Scanner;

public class SherlockAndTheBeast {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int n = in.nextInt();
			System.out.println(processNumber(n));
		}
		in.close();
	}

	static String processNumber(int num) {
		int a = num;
		
		while (a % 3 != 0) {
			a -= 5;
			if (a < 0) {
				return "-1";
			}
		}

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < a; i++) {
			sb.append("5");
		}
		for (int i = 0; i < num - a; i++) {
			sb.append("3");
		}
		return sb.toString();
	}

}
