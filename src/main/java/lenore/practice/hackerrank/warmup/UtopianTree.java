package lenore.practice.hackerrank.warmup;

import java.util.Scanner;

public class UtopianTree {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int t = in.nextInt();
    	
    	for (int i = 1; i <= t; i++){
    		int height = 1; 
    		int age = in.nextInt();
    		for (int lc = 1; lc <= age; lc++){
    			if (lc % 2 == 0) {
    				height += 1;
    			}
    			else {
    				height *= 2;
    			}
    		}
    		System.out.println("height is :" + height);
    	}
    	in.close();
    }
}
