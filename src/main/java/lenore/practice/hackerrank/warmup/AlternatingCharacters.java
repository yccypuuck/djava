package lenore.practice.hackerrank.warmup;

import java.util.Scanner;

public class AlternatingCharacters {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t;

		String _a;
		t = in.nextInt();
		for (int i = 0; i < t; i++) {
			_a = in.next();
			int deletions = calcDeletions(_a);
			System.out.println(deletions);
		}
		in.close();
	}

	private static int calcDeletions(String data) {
	    final char[] copy = data.toCharArray();
	    int i = 0;
	    int j = 1;
	    int deletions = 0;
	    while (i < copy.length && j < copy.length) {
	    	if (copy[i] == copy[j]) {
	    		deletions++;
	    		j++;
	    	}
	    	else {
	    		i = j++;
	    	}
	    }
		return deletions;
	}
}
