package lenore.practice.hackerrank.warmup;

import java.util.Scanner;

public class TheLoveLetterMystery {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t;

		String _a;
		t = in.nextInt();
		for (int i = 0; i < t; i++) {
			_a = in.next();
			int alterations = toPalindrome(_a);
			System.out.println(alterations);
		}
		in.close();
	}

	private static int toPalindrome(String data) {
		int changes = 0;
	    final char[] copy = data.toCharArray();
	    int i = 0;
	    int j = copy.length - 1;
	    while (i < j) {
	    	if (copy[i] > copy[j]) {
	    		while (copy[i] != copy[j]) {
	    			changes++;
	    			copy[i] = (char)(copy[i]-1);
	    		}
	    	}
	    	else {
	    		while (copy[i] != copy[j]) {
	    			changes++;
	    			copy[j] = (char)(copy[j]-1);
	    		}
	    	}
	    	i++;
	    	j--;
	    }
		
		return changes;
	}
}
