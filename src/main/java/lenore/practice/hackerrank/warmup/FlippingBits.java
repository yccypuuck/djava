package lenore.practice.hackerrank.warmup;

import java.util.Scanner;

public class FlippingBits {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = 1;
        int t = input.nextInt();
        for (int i = 0; i < t; i++) {
            //number  = input.nextInt();
        	long inverted = (long) ~number ;
        	System.out.println(inverted & 0xffffffff);
        	inverted = inverted & 0xffffffff;
            System.out.println(Long.toBinaryString(inverted));
        }
        input.close();
    }
}
