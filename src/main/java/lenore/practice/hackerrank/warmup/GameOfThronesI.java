package lenore.practice.hackerrank.warmup;

import java.util.Scanner;

public class GameOfThronesI {

	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		String inputString = myScan.nextLine();
		
		int [] charMap = new int [256];
	    final char[] copy = inputString.toCharArray();

		for (char aCopy : copy) {
			int value = (int) aCopy;
			charMap[value]++;
		}
		boolean result = isPalindrome(charMap);
		String ans = result?"YES":"NO";
		// Assign ans a value of YES or NO, depending on whether or not
		// inputString satisfies the required condition
		System.out.println(ans);
		myScan.close();
	}
	
	private static boolean isPalindrome (int[] charMap) {
		boolean isPalindrome = false;
		for (int aCharMap : charMap) {
			if (aCharMap % 2 != 0) {
				if (isPalindrome) {
					return false;
				}
				isPalindrome = true;
			}
		}
		return true;
	}
}
