package lenore.practice.leetcode;

public class IntegerToRomanSlow {
	private static char[] letters = {'M','D','C','L','X','V','I'};

	public String intToRoman(int num) {
		if (num > 3999) {
			return "Unexpected value";
		}
		StringBuilder result = new StringBuilder();
		for (int i = 1000, c = 0; i > 0; i = i/10, c = c+2) {
			result.append(printPart(c, num/i));
			num %= i;
			
		}
		result.append(printPart(6, num));
		
		return result.toString();
	}

	
	private String printChar(int position, int counter) {
		String result = "";
		for (int i = counter; i > 0; i--){
			result += letters[position];
		}
		return result;
	}
	
	private String printPart(int position, int counter) {
		if (counter == 9) {
			return "" + letters[position] + letters[position - 2];
		} 
		if (counter >= 5) {
			return letters[position - 1] + printChar(position, counter - 5); 
		}
		if (counter == 4) {
			return "" + letters[position] + letters[position - 1];
		}
		return printChar(position, counter);
	}

}
