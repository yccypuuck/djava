package lenore.practice.leetcode;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

public class ContainerWithMostWater {
	private void setLevel(Level level) {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration conf = ctx.getConfiguration();
		conf.getLoggerConfig(LogManager.ROOT_LOGGER_NAME).setLevel(level);
		ctx.updateLoggers(conf);
	}

	public static Logger logger = LogManager.getLogger(RegExpMatching.class.getName());

	{
		setLevel(Level.DEBUG);
		setLevel(Level.INFO);
	}


	public int maxArea(int[] height) {
		if (height == null)
			return 0;
		int maxArea = 0;
		int l = 0;
		int r = height.length - 1;
		while (l < r) {
			int tempArea = Math.min(height[l], height[r]) * (r-l);
			maxArea = Math.max(tempArea, maxArea);
			if (height[l] > height[r]) {
				r--;
			} else {
				l++;
			}
			
		}
		return maxArea;
	}

	public static void main(String[] args) {
		ContainerWithMostWater tester = new ContainerWithMostWater();

		logger.info(tester.maxArea(new int[] { 2, 3, 10, 5, 7, 8, 9 }));

	}
}
