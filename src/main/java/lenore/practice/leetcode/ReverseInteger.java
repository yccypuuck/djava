package lenore.practice.leetcode;

public class ReverseInteger {

	public static void main(String[] args) {
		ReverseInteger runner = new ReverseInteger();
		System.out.println(runner.reverse(-10302));
		System.out.println(runner.reverse(1000000003));
	}

	public int reverse(int x) {
	int carrier = 1;
		if (x < 0) {
			carrier = -1;
			x *= -1;
		}
		int result = 0;
		while (x > 0) {
			if (result > Integer.MAX_VALUE / 10) {
				return 0;
			}
			result *= 10;
			result += x % 10;
			x = x / 10;
		}
		return result * carrier;
	}
}
