package lenore.practice.leetcode;

import lenore.utils.ds.node.NodeImpl;

/**
 * 
 * @author lenore
 *
 *         You are given two linked lists representing two non-negative numbers.
 *         The digits are stored in reverse order and each of their nodes
 *         contain a single digit. Add the two numbers and return it as a linked
 *         list. Input: (2 -> 4 -> 3) + (5 -> 6 -> 4) Output: 7 -> 0 -> 8
 */

public class AddTwoNumbers {

	public static NodeImpl addTwoNumbers(NodeImpl l1, NodeImpl l2) {
		NodeImpl node = new NodeImpl(0);
		NodeImpl sum = node;
		int carrier;
		while (l1 != null || l2 != null) {
			if (l1 != null) {
				sum.data += l1.data;
				l1 = l1.next;
			}
			if (l2 != null) {
				sum.data += l2.data;
				l2 = l2.next;
			}

			carrier = sum.data / 10;
			if (carrier > 0) {
				sum.data = sum.data % 10;
			}
			if (carrier != 0 || l1 != null || l2 != null) {
			    sum.next = new NodeImpl(carrier);
			    sum = sum.next;
			}
		}

		return node;   
	}

	public static void main(String[] args) {
		NodeImpl l1 = new NodeImpl(5);
		NodeImpl l2 = new NodeImpl(5);
		NodeImpl sum = addTwoNumbers(l1, l2);
		sum.printList();
		
		l1 = new NodeImpl(new int[] {1,6,0,3,3,6,7,2,0,1});
		l2 = new NodeImpl(new int[] {6,3,0,8,9,6,6,9,6,1});
		sum = addTwoNumbers(l1, l2);
		sum.printList();
				
	}

}
