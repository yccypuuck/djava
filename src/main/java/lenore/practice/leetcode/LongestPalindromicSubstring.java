package lenore.practice.leetcode;

public class LongestPalindromicSubstring {

	public static void main(String[] args) {
		LongestPalindromicSubstring finder = new LongestPalindromicSubstring();
		System.out.println(finder.longestPalindrome("ccc"));
		System.out.println(finder.longestPalindrome("supposes"));
	}

	public String longestPalindrome(String s) {
		if (s == null || s.length() == 1) {
			return s;
		}
		char[] template = preProcess(s);
		int[] ptn = new int[template.length];
		int longest = 0;
		int index = 0;
		for (int i = 1; i < template.length - 1; i++) {
			int left = i - 1;
			int right = i + 1;
			while (left >= 0 && right < template.length
					&& template[left] == template[right]) {
				ptn[i]++;
				left--;
				right++;
			}
			if (ptn[i] > longest) {
				index = i;
				longest = ptn[i];
			}
		}
		int start = (index - longest) / 2;
		return s.substring(start, start + longest);
	}

	private char[] preProcess(String s) {
		char[] result = new char[s.length() * 2 + 3];
		result[0] = '$';
		result[s.length() * 2 + 2] = '@';
		for (int i = 0; i < s.length(); i++) {
			result[2 * i + 1] = '#';
			result[2 * i + 2] = s.charAt(i);
		}
		result[s.length() * 2 + 1] = '#';
		return result;
	}
}
