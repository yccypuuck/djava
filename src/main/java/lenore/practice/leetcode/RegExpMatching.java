package lenore.practice.leetcode;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;

/**
 * '.' Matches any single character. 
 * '*' Matches zero or more of the preceding element. The matching should cover the entire input string (not partial). 
 * The function prototype should be: 
 * bool isMatch(const char *s, const char *p) 
 * Some examples: 
 * isMatch("aa","a") → false 
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false 
 * isMatch("aa", "a*") → true 
 * isMatch("aa", ".*") → true 
 * isMatch("ab", ".*") → true 
 * isMatch("aab", "c*a*b") → true
 * 
 * @author lenore
 *
 */

public class RegExpMatching {
	private void setLevel(Level level) {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration conf = ctx.getConfiguration();
		conf.getLoggerConfig(LogManager.ROOT_LOGGER_NAME).setLevel(level);
		ctx.updateLoggers(conf);
	}

	public static Logger logger = LogManager.getLogger(RegExpMatching.class.getName());

	private String s;
	private String p;

	public boolean isMatch(String s, String p) {
		setLevel(Level.DEBUG);
		setLevel(Level.INFO);

		this.s = s;
		this.p = p;
		logger.debug(String.format("checking %s on %s", s, p));

		if (s != null && p != null && s.length() == 0 && p.length() == 0)
			return true;
		if (s == null || p == null || p.length() == 0 || p.charAt(0) == '*' || p.contains("**"))
			return false;

		return !(p.indexOf('*') < 0 && s.length() != p.length()) && isMatch(0, 0);

	}

	private boolean isMatch(int sRunner, int pRunner) {
		boolean result;
		if (sRunner == s.length() && !p.endsWith("*")) {
			logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner == s.length() && !p.endsWith('*')", sRunner,
					pRunner));
			return false;
		}
		if (pRunner == p.length() && sRunner < s.length()) {
			logger.debug(String.format("sRunner:%d, pRunner:%d. pRunner == p.length() && sRunner < s.length()", sRunner,
					pRunner));
			return false;
		}
		switch (p.charAt(pRunner)) {
		case '.':
			logger.debug(String.format("sRunner:%d, pRunner:%d. case '.'", sRunner, pRunner));
			pRunner++;
			if (pRunner == p.length()) {
				logger.debug(String.format("sRunner:%d, pRunner:%d. pRunner == p.length()", sRunner, pRunner));
				sRunner++;
				break;
			}

			if (p.charAt(pRunner) != '*') {
				if (sRunner == s.length()) {
					logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner == s.length()", sRunner, pRunner));
					return false;
				}
				logger.debug(String.format("sRunner:%d, pRunner:%d. p.charAt(pRunner) != '*'", sRunner, pRunner));
				sRunner++;
				return isMatch(sRunner, pRunner);
			}

			pRunner++;
			if (pRunner == p.length()) {
				logger.debug(String.format("sRunner:%d, pRunner:%d. pRunner == p.length()", sRunner, pRunner));
				return true;
			}

			result = false;
			while (sRunner < s.length()) {
				result = result || isMatch(sRunner, pRunner);
				sRunner++;
				if (result)
					return true;
			}

			return isMatch(sRunner, pRunner);
		default:
			logger.debug(String.format("sRunner:%d, pRunner:%d. default case", sRunner, pRunner));
			char test = p.charAt(pRunner);
			pRunner++;

			if (pRunner == p.length()) {
				logger.debug(String.format("sRunner:%d, pRunner:%d. pRunner == p.length()", sRunner, pRunner));
				if (sRunner == s.length() || s.charAt(sRunner) != test) {
					logger.debug(
							String.format("sRunner:%d, pRunner:%d. sRunner == s.length() || s.charAt(sRunner) != test",
									sRunner, pRunner));
					return false;
				}
				sRunner++;
				break;
			}

			if (p.charAt(pRunner) != '*') {
				logger.debug(String.format("sRunner:%d, pRunner:%d. p.charAt(pRunner) != '*'", sRunner, pRunner));
				if (sRunner >= s.length() || s.charAt(sRunner) != test) {
					logger.debug(
							String.format("sRunner:%d, pRunner:%d. sRunner >= s.length() || s.charAt(sRunner) != test",
									sRunner, pRunner));
					return false;
				}
				sRunner++;
				return isMatch(sRunner, pRunner);
			}

			pRunner++;
			if (sRunner == s.length()) {
				logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner == s.length()", sRunner, pRunner));
				if (pRunner == p.length()) {
					logger.debug(String.format("sRunner:%d, pRunner:%d. pRunner == p.length()", sRunner, pRunner));
					break;
				}
				logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner == s.length()", sRunner, pRunner));
				return isMatch(sRunner, pRunner);
			}
			if (s.charAt(sRunner) != test) {
				logger.debug(String.format("sRunner:%d, pRunner:%d. s.charAt(sRunner) != test", sRunner, pRunner));
				return isMatch(sRunner, pRunner);
			}

			result = false;
			do {
				logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner < s.length() && s.charAt(sRunner) == test",
						sRunner, pRunner));
				result = result || isMatch(sRunner++, pRunner);
				if (result)
					return true;

			} while (sRunner < s.length() && s.charAt(sRunner) == test);

			if (sRunner >= s.length() && pRunner >= p.length()) {
				return true;
			}
			return isMatch(sRunner, pRunner);
		}

		if (sRunner < s.length()) {
			logger.debug(String.format("sRunner:%d, pRunner:%d. sRunner < s.length()", sRunner, pRunner));
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		RegExpMatching matcher = new RegExpMatching();
		logger.info(matcher.isMatch("aabcbcbcaccbcaabc", ".*a*aa*.*b*.c*.*a*") + " " + true + ":\t aabcbcbcaccbcaabc "
				+ ".*a*aa*.*b*.c*.*a*");
		logger.info(matcher.isMatch("aa", "a*") + " " + true + ":\t aa " + "a*");
		logger.info(matcher.isMatch("aa", "aa*a") + " " + true + ":\t aa " + "aa*a");
		logger.info(matcher.isMatch("a", "ab*") + " " + true + ":\t a " + "ab*");
		logger.info(matcher.isMatch("ab", ".*") + " " + true + ":\t ab " + ".*");
		logger.info(matcher.isMatch("aab", "c*a*b") + " " + true + ":\t aab " + "c*a*b");
		logger.info(matcher.isMatch("aaa", "ab*a*c*a") + " " + true + ":\t aaa " + "ab*a*c*a");
		logger.info(matcher.isMatch("aaa", "aa*a") + " " + true + ":\t aaa " + "aa*a");
		logger.info(matcher.isMatch("aasdfasdfasdfasdfas", "aasdf.*asdf.*asdf.*asdf.*s") + " " + true
				+ ":\t aasdfasdfasdfasdfas " + "aasdf.*asdf.*asdf.*asdf.*s");
		logger.info(matcher.isMatch("aa", "aa") + " " + true + ":\t aa " + "aa");
		logger.info(matcher.isMatch("abcadaddea", "ab.*a") + " " + true + ":\t abcadaddea " + "ab.*a");
		logger.info(matcher.isMatch("", "") + " " + true + ":\t - " + "-");
		logger.info(matcher.isMatch("abcd", "d*") + " " + false + ":\t abcd " + "d*");
		logger.info(matcher.isMatch("aca", "a*a") + " " + false + ":\t aca " + "a*a");
		logger.info(matcher.isMatch("", "aa") + " " + false + ":\t - " + "aa");
		logger.info(matcher.isMatch("abcdea", "aba.*a") + " " + false + ":\t abcdea " + "aba.*a");
		logger.info(matcher.isMatch("ab", "b.*") + " " + false + ":\t ab " + "b.*");
		logger.info(matcher.isMatch("ab", "*") + " " + false + ":\t ab " + "*");
		logger.info(matcher.isMatch(null, null) + " " + false + ":\t null " + "null");
		logger.info(matcher.isMatch("a", "aa") + " " + false + ":\t a " + "aa");
		logger.info(matcher.isMatch("aa", "a") + " " + false + ":\t aa " + "a");
		logger.info(matcher.isMatch("aaa", "aa") + " " + false + ":\t aaa " + "aa");
		logger.info(matcher.isMatch("a", ".*..a*") + " " + false + ":\t a " + ".*..a*");
		logger.info(matcher.isMatch("acaabbaccbbacaabbbb", "a*.*b*.*a*aa*a*") + " " + false + ":\t acaabbaccbbacaabbbb "
				+ "a*.*b*.*a*aa*a*");
	}
}
