package lenore.practice.leetcode;

public class StringToInteger {

	public static void main(String[] args) {
		StringToInteger runner = new StringToInteger();
		// System.out.println(runner.atoi("-98"));
		// System.out.println(runner.atoi("-+1"));
		// System.out.println(runner.atoi("  -0012a42"));
		// System.out.println(runner.atoi("   +0 123"));
		System.out.println("2147483647 \t: " + runner.atoi("2147483648"));
		System.out.println("-2147483648 \t: " + runner.atoi("    -10326662300y"));
		System.out.println(runner.atoi("-2147483647"));
		// System.out.println(runner.atoi("10522545459"));
	}

	public int atoi(String str) {
		if (str == null || str.length() == 0) {
			return 0;
		}
		str = str.trim();
		int i = 0;
		int carrier = 1;
		if (str.charAt(i) == '-') {
			carrier = -1;
			i++;
		} else if (str.charAt(i) == '+') {
			i++;
		}
		Integer result = 0;
		while (i < str.length()) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return result * carrier;
			}
			Integer value = Integer.parseInt(str.substring(i, i + 1));
			if (carrier == 1
					&& (Integer.MAX_VALUE - value - result * 10 < 0 || result.toString().length() == 10)) {
				return Integer.MAX_VALUE;
			}
			if (carrier == -1
					&& (Integer.MIN_VALUE + result * 10 + value > 0 || result.toString().length() == 10)) {
				return Integer.MIN_VALUE;
			}
			result *= 10;
			result += value;
			i++;
		}
		return result * carrier;
	}
}
