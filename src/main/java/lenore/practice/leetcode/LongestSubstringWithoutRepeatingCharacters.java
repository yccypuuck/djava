package lenore.practice.leetcode;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {

    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
    	char [] str = s.toCharArray();
    	Set<Character> charSet = new HashSet<>();
    	int [] prefix = new int [s.length()];
    	int runner = 0;
    	prefix[runner] = 1;
    	charSet.add(str[runner]);
    	for (int i = 1; i < str.length; i++) {
    		if (str[i] != str[runner] && !charSet.contains(str[i])) {
    			charSet.add(str[i]);
    			prefix[i] = prefix[i-1] +1;
    		} else if (str[i] == str[runner]){
    			prefix[i] = prefix[i-1];
    			runner++;
    		} else {
    			prefix[i] = prefix[i-1];
    			while (str[i] != str[runner]) {
    				charSet.remove(str[runner]);
    				prefix[i]--;
    				runner++;
    			} 
    			runner++;
    		}
    	}
    	
    	int max = 0;
		for (int aPrefix : prefix) {
			if (aPrefix > max) {
				max = aPrefix;
			}
		}
    	return max;
    }

}
