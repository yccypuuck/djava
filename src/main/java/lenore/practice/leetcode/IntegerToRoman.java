package lenore.practice.leetcode;

public class IntegerToRoman {
	private static int[] numbers =       {1000,900, 500,400, 100,90,  50, 40,  10, 9,   5,  4,   1};
	private static String[] letters = {"M", "CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};

	public static String intToRoman(int num) {
		StringBuilder result = new StringBuilder();
		int i = 0;
		while (num > 0) {
			int times = num/ numbers[i];
			for (; times > 0; times--) {
				result.append(letters[i]);
			}
			num = num % numbers[i];
			i++;
		}
		return result.toString();
	}

	public static void main(String[] args) {
		for (int i = 1; i < 4000; i++) {
			System.out.print(intToRoman(i) + " ");
		}
	}
}
