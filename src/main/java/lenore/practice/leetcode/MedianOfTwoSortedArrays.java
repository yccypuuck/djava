package lenore.practice.leetcode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MedianOfTwoSortedArrays {
	public static void main(String[] args) {

	}

	int[] a;
	int[] b;
    public final static Logger logger = LogManager.getLogger(MedianOfTwoSortedArrays.class.getName());


	public double findMedianSortedArrays(int A[], int B[]) {
		if (A == null) {
			return B[B.length/2];
		}
		if (B == null) {
			return A[A.length/2];
		}
		if (A.length <= B.length) {
			this.a = A;
			this.b = B;
		}
		else {
			this.b = A;
			this.a = B;
		}
		
		return findMedian(new int[] { 0, a.length - 1 }, new int[] { 0, b.length - 1 });
	}

	private double findMedian(int[] nL, int[] mL) {
		logData("Initial data", nL, mL);
		int N = nL[1] - nL[0] + 1;
		int M = mL[1] - mL[0] + 1;
		int nMedian = nL[0] + (N / 2) - ((N % 2 == 0) ? 1 : 0);
		int mMedian = mL[0] + (M / 2);
		if (N < 0 || M < 0) {
			return 0;
		}
		if (N == 0) {
			if (M % 2 != 0) {
				return b[mMedian];
			}
			return medianOfTwo(b[mMedian], b[mMedian-1]);
		}
		
		if (N == 1) {
			logData("N array has one element", nL, mL);
			/**
			 * Case 1: There is only one element in both arrays, so output the average of A[0] and B[0].
			 */
			if (M == 1) {
				return medianOfTwo(a[nL[0]], b[mL[0]]);
			}

			/**
			 * Case 2: N = 1, M is odd
			 * First find the middle element of B[], which is 12 for above array. There are following 4 sub-cases.
			 * ...2.1 If A[0] is smaller than 10, the median is average of 10 and 12.
			 * ...2.2 If A[0] lies between 10 and 12, the median is average of A[0] and 12.
			 * ...2.3 If A[0] lies between 12 and 15, the median is average of 12 and A[0].
			 * ...2.4 If A[0] is greater than 15, the median is average of 12 and 15.
			 * In all the sub-cases, we find that 12 is fixed. 
			 * So, we need to find the median of B[ M / 2 - 1 ], B[ M / 2 + 1], A[ 0 ] 
			 * and take its average with B[ M / 2 ].
			 */
			if (M % 2 != 0) {
				
				return medianOfTwo(b[mMedian], medianOfThree(b[mMedian-1], b[mMedian+1], a[nMedian]));

			}
			/**
			 * Case 3: N = 1, M is even
			 * First find the middle items in B[], which are 10 and 12 in above example. There are following 3 sub-cases.
			 * ...3.1 If A[0] is smaller than 10, the median is 10.
			 * ...3.2 If A[0] lies between 10 and 12, the median is A[0].
			 * ...3.3 If A[0] is greater than 10, the median is 12.
			 * So, in this case, find the median of three elements B[ M / 2 - 1 ], B[ M / 2] and A[ 0 ].
			 */
			return medianOfThree(b[mMedian - 1], b[mMedian], a[nL[0]]);
		}
		if (N == 2) {
			logData("N array has two elements", nL, mL);
			/**
			 * Case 4: N = 2, M = 2
			 * There are four elements in total. So we find the median of 4 elements.
			 */
			if (M == 2) {
				return medianOfFour(a[nL[0]], a[nL[1]], b[mL[0]], b[mL[1]]);
			}
			/**
			 * Case 5: N = 2, M is odd
			 * The median is given by median of following three elements: 
			 * B[M/2], max(A[0], B[M/2 - 1]), min(A[1], B[M/2 + 1]).
			 */
			if (M % 2 != 0) {
				return medianOfThree(b[mMedian], max(a[nL[0]], b[mMedian-1]), min(a[nL[1]],b[mMedian+1]));
			}
			/**
			 * Case 6: N = 2, M is even
			 * The median is given by median of following four elements: 
			 * B[M/2], B[M/2 - 1], max(A[0], B[M/2 - 2]), min(A[1], B[M/2 + 1])
			 */
			return medianOfFour(b[mMedian],b[mMedian-1], max(a[nL[0]], b[mMedian-2]), min(a[nL[1]],b[mMedian+1]));
		}
		/**
		 * Remaining cases:
		 * 1) Find the middle item of A[] and middle item of B[].
		 * ...1.1) If the middle item of A[] is greater than middle item of B[], ignore the last half of A[], 
		 * let length of ignored part is idx. Also, cut down B[] by idx from the start.
		 * ...1.2) else, ignore the first half of A[], let length of ignored part is idx. 
		 * Also, cut down B[] by idx from the last.
		 */
		if (a[nMedian] > b[mMedian]) {
			mL[0] += nL[1] - nMedian;
			nL[1] = nMedian;
			logData("N median is greater", nL, mL);
			return findMedian(nL, mL);
		} else {
			mL[1] -= nMedian - nL[0];
			nL[0] = nMedian;
			logData("M median is greater", nL, mL);
			return findMedian(nL, mL);
		}
	}

	private void logData(String text, int[] nL, int[] mL) {
		logger.info(String.format("=== %s === n: [%d,%d] m: [%d,%d]", text, nL[0], nL[1], mL[0], mL[1]));
	}

	private int min(int a, int b) {
		return a < b ? a : b;
	}

	private int max(int a, int b) {
		return a > b ? a : b;
	}

	private double medianOfTwo(int a, int b) {
		return (a + b) / 2.0;
	}

	private int medianOfThree(int a, int b, int c) {
		return a + b + c - min(a, min(b, c)) - max(a, max(b, c));
	}

	private double medianOfFour(int a, int b, int c, int d) {
		int min = min(a, min(b, min(c, d)));
		int max = max(a, max(b, max(c, d)));
		return (a + b + c + d - min - max) / 2.0;
	}

}
