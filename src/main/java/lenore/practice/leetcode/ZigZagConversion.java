package lenore.practice.leetcode;

public class ZigZagConversion {

	/**
	 * The string "PAYPAGOSHORONG" is written in a zigzag pattern on a given number of rows like this: 
	 * <br/>
	 * P&nbsp;&nbsp;&nbsp;
	 * G&nbsp;&nbsp;&nbsp;&nbsp;
	 * H&nbsp;&nbsp;&nbsp;
	 * N<br/>
	 * A&nbsp;P&nbsp;G&nbsp;S&nbsp;O&nbsp;O&nbsp;G<br/>
	 * Y&nbsp;&nbsp;&nbsp;
	 * O&nbsp;&nbsp;&nbsp;
	 * R<br/>
	 * 
	 * And then read line by line: "PAHNAPLSOOGYOR"
	 * Write the code that will take a string and make this conversion given a number of rows:
	 * 
	 * @param s - input string
	 * @param nRows - number of rows. should be more than 2 to work
	 * @return converted string
	 */
	public String convert(String s, int nRows) {
		if (s == null) {
			return "";
		}
		if (s.length() <= 2 || nRows <= 1) {
			return s;
		}
		StringBuilder[] list = new StringBuilder[nRows];
		for (int i = 0; i < list.length; i++) {
			list[i] = new StringBuilder();
		}
		int row = 0;
		int runner = 0;
		boolean goesDown = true;
		while (runner < s.length()) {
			if (row == 0) {
				goesDown = true;
			}
			if (row == nRows - 1) {
				goesDown = false;
			}
			list[row].append(s.charAt(runner));
			if (goesDown) {
				row++;
			} else {
				row--;
			}
			runner++;
		}
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < nRows; i++) {
			result.append(list[i]);
		}
		return result.toString();
	}
}
