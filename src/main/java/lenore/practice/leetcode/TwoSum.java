package lenore.practice.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lenore
 * 
 *         Given an array of integers, find two numbers such that they add up to
 *         a specific target number. The function twoSum should return indices
 *         of the two numbers such that they add up to the target, where index1
 *         must be less than index2. Please note that your returned answers
 *         (both index1 and index2) are not zero-based. You may assume that each
 *         input would have exactly one solution. Input: numbers={2, 7, 11, 15},
 *         target=9 Output: index1=1, index2=2
 */

public class TwoSum {
	public static int[] twoSum(int[] numbers, int target) {
		int[] indices = new int[2];
		Map<Integer, Integer> map = new HashMap<>();
		int i = 0;
		while (i < numbers.length) {
			Integer lookup = map.get(target - numbers[i]);
			if (lookup != null) {
				indices[0] = lookup;
				indices[1] = ++i;
				return indices;
			}
			map.put(numbers[i], ++i);
		}
		return null;
	}

	public static void main(String[] args) {
		int[] numbers = { 2, 7, 11, 15 };
		int target = 9;
		int[] indices = twoSum(numbers, target);
		if (indices != null) {
			System.out.println(indices[0] + ", " + indices[1]);
		}

	}

}
