package lenore.interview.google;

import java.util.HashMap;
import java.util.Map;

public class CounterImpl <T> implements Counter<T>{
	private Map<T, Integer> map = new HashMap<>();

	public CounterImpl() {
		map = new HashMap<>();
	}

	public Map<T,Integer> getMap() {
		return map;
	}

	public void decreaseValue(T key) {
		Integer value = map.get(key);
		if (value == null) {
			value = 0;
		}
		map.put(key, --value);
	}

	public void increaseValue(T key) {
		Integer value = map.get(key);
		if (value == null) {
			value = 0;
		}
		map.put(key, ++value);
	}

	public boolean isEmpty() {
		for (T key : map.keySet()) {
			if (this.contains(key)) {
				return false;
			}
		}
		return true;
	}

	public boolean contains(T key) {
		Integer value = map.get(key);
		return !(value == null || value == 0);
	}
}
