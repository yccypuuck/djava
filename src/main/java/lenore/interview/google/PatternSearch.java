package lenore.interview.google;

public class PatternSearch {
	public int isAnagram(char[] pattern, char[] text) {
		Counter<Character> dictPattern = new CounterImpl<Character>();
		for (int i = 0; i < pattern.length; i++) {
			dictPattern.increaseValue(pattern[i]);
			dictPattern.decreaseValue(text[i]);
		}
		int runner = pattern.length;
		while (runner < text.length) {
			if (dictPattern.isEmpty()) {
				return runner - pattern.length;
			}
			int head = runner - pattern.length; 
			dictPattern.increaseValue(text[head]);
			dictPattern.decreaseValue(text[runner]);
			runner++;
		}  
		return -1;
	}
	
	public static void main(String [] args) {
		PatternSearch searcher = new PatternSearch();
		System.out.println(searcher.isAnagram("abc".toCharArray(), "b6caaabb??cabba".toCharArray()));
	}
}
