package lenore.interview.google;

public class ValidateBinarySearchTree {
	private class Node {
		Node left;
		Node right;
		int value;
	}

	/**
	 * Test cases:
	 * 1 : values in left sub-tree are less than current node && values in right sub-tree are greater than current node 
	 * 2 : null tree
	 * 3 : duplicate values 
	 * 4 : broken binary tree (loops, two nodes have same child) 
	 * 5 : Integer min & max as valid numbers
	 * 
	 */

	public boolean isValid(Node root) {
		if (root == null) {
			return true;
		}
		// both subtrees should be valid
		return isValidSubTree(root.left, Integer.MIN_VALUE, root.value)
				&& isValidSubTree(root.right, root.value, Integer.MAX_VALUE);
	}

	private boolean isValidSubTree(Node root, int min, int max) {
		// valid if root is null
		if (root == null) {
			return true;
		}

		// root.value has to be in range:
		// 1. (min;max)
		// 2. [integer.min_value;max) or (min;integer.max_value]
		// define simple left & right from complicated situation
		int left = (min == Integer.MIN_VALUE) ? Integer.MIN_VALUE : min + 1;
		int right = (max == Integer.MAX_VALUE) ? Integer.MAX_VALUE : max - 1;

		// invalid if root is outside boundaries
		if (root.value < left || root.value > right) {
			return false;
		}
		// recursive processing
		return isValidSubTree(root.left, min, root.value)
				&& isValidSubTree(root.right, root.value, max);
	}
}
