package lenore.interview.google;

import java.util.Map;

public interface Counter<T> {

	void decreaseValue(T key);

	void increaseValue(T key);

	boolean isEmpty();

	Map<?,Integer> getMap();

	boolean contains(T key);
}
