package lenore.interview.intuit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrintX {

	public static void printX(int num) {
		System.out.println("the input number is: " + num);
		if (num < 0) {
			System.out.println("you've entered non positive number");
			return;
		}
		if (num % 2 == 0) {
			System.out.println("you've entered even number");
			return;
		}
		for (int i = 1; i <= num; i++) {
			for (int j = 1; j <= num; j++) {
				if (j == i || j + i == num + 1) {
					System.out.print("X");
				} else {
					System.out.print("O");
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		boolean isRunning = false;
		do {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
				System.out.println("enter a positive odd integer");
				int num = Integer.parseInt(br.readLine());
				printX(num);

				System.out.println("enter yes to try again");
				String answer = br.readLine();
				if (answer.equals("yes")) {
					isRunning = true;
				}
			} catch (IOException e) {
				System.out.println("Something went wrong..");
				isRunning = false;
			}
		} while (isRunning);
	}
}
