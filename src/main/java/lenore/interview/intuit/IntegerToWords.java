package lenore.interview.intuit;

public class IntegerToWords {

	private String[] tens = { "", "", "twen", "thir", "four", "fif", "six", "seven", "eigh", "nine" };
	private String[] ones = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
			"eleven", "twelve" };

	public String convertToText(int num) {
		if (Math.abs(num) > 100) {
			return num + " is not supported";
		}
		if (num == 0) {
			return "zero";
		}
		
		if (num < 0) {
			return "minus " + convertToText(-num);
		}
		if (num == 100) {
			return "one hundred";
		}
		// tens and their ones
		if (num / 10 > 1) {
			String result = tens[num / 10] + "ty";
			int one = num % 10;
			if (one > 0) {
				result += " " + ones[one];
			}
			return result;
		}
		// teens
		if (num > 12) {
			return tens[num % 10] + "teen";
		}
		// simplest numbers
		return ones[num];
	}

	public static void main(String[] args) {
		IntegerToWords convertor = new IntegerToWords();
		for (int i = -101; i <= 101; i++) {
			System.out.print(convertor.convertToText(i) + ", ");
			if (Math.abs(i % 10) == 0) {
				System.out.println();
			}
		}
	}

}
