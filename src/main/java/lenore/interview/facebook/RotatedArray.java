package lenore.interview.facebook;

public class RotatedArray {
	private int[] nums;
	private int x;
	
	public boolean binaryLookUp(int [] nums, int x) {
		if (nums == null) {
			return false;
		}
		this.x = x;
		this.nums = nums;
		return binaryLookUp(0, nums.length - 1);
	}
	
	private boolean binaryLookUp (int left, int right) {
		if (right == left) {
			return false;
		}

		if (x < nums[left] && x > nums[right]) {
			return false;
		}

		int pivot = left + (right - left) / 2;
		return nums[left] == x || nums[right] == x || nums[pivot] == x || binaryLookUp(++left, pivot - 1) || binaryLookUp(pivot + 1, --right);

	}

	public static void main(String [] args) {
		RotatedArray searcher = new RotatedArray();
		int[] input = { 5, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 1, 2, 3, 4 };
		System.out.println(searcher.binaryLookUp(input, 17));
	}	

}
