package lenore.utils.sort;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * Created by ryzma01 on 5/22/2014.
 *
 * Abstract class for all sorting algorithms.
 * Has two abstract methods:
 * - sort whole array - public method
 * - sort in given boundaries - protected array for inner use
 *
 */
abstract public class Sort {

    abstract protected void sort(int left, int right);

    public final static Logger logger = LogManager.getLogger(Sort.class.getName());

    protected Number[] inputNumbers;

    public long start, end;

    public Sort() { }
    public Sort(Number[] input) {
        setInputNumbers(input);
    }

    public void setInputNumbers(Number [] input) {
        inputNumbers = input;
    }

    // Initial sort, not to mess with "out of bounds" possibility
    public Number[] sort() {

        if (inputNumbers == null || inputNumbers.length == 0) {
            return inputNumbers;
        }

        start = System.nanoTime();
        sort(0, inputNumbers.length - 1);
        end = System.nanoTime();
        logger.debug(String.format("%d ms, n=%d : %s sort", end - start, inputNumbers.length, logger.getName()));

        return inputNumbers;
    }

    public void printArray(Number[] values){
        logger.info("Values: " + Arrays.toString(values));
    }
    public void printArray(){
        logger.info("Values: " + Arrays.toString(inputNumbers));
    }

    public String toString(){
        StringBuilder array = new StringBuilder();
        for (Number value : inputNumbers) {
            array.append("[").append(value).append("] ");
        }
        return array.toString();
    }

}
