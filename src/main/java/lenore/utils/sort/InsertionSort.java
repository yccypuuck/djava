package lenore.utils.sort;

public class InsertionSort extends Sort {

	public InsertionSort() { }
	public InsertionSort(Number[] input) {
		setInputNumbers(input);
	}

	@Override
	protected void sort(int left, int right) {
		for (int i = left + 1; i <= right; i++) {
			if (inputNumbers[i].intValue() < inputNumbers[i - 1].intValue()) {
				swap(i, left);
			}
		}
	}

	public void swap(int index, int left) {
		while (index > left && inputNumbers[index].intValue() < inputNumbers[index - 1].intValue()) {
			Number temp = inputNumbers[index];
			inputNumbers[index] = inputNumbers[index - 1];
			inputNumbers[index - 1] = temp;
			index--;
		}
	}

	public static Number[] sort(Number[] input) {
		InsertionSort sorter = new InsertionSort(input);
		return sorter.sort();
	}

}
