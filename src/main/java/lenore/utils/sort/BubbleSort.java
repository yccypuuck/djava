package lenore.utils.sort;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BubbleSort extends Sort {

	private final static Logger logger = LogManager.getLogger(BubbleSort.class.getName());


	public BubbleSort() { }
	public BubbleSort(Number[] input) {
		setInputNumbers(input);
	}

	@Override
	public Number[] sort() {
		if (inputNumbers == null || inputNumbers.length == 0) {
			return inputNumbers;
		}
		start = System.nanoTime();
		sort(0,inputNumbers.length);
		end = System.nanoTime();
		logger.debug(String.format("%d ms, n=%d : Bubble sort", end - start, inputNumbers.length));
		return inputNumbers;
	}

	@Override
	protected void sort(int left, int right) {
		boolean swapped = true;
		while (swapped) {
			swapped = false;
			for (int i = 1; i < inputNumbers.length; i++) {
				if (inputNumbers[i].intValue() < inputNumbers[i-1].intValue()) {
					swap(i);
					swapped = true;
				}
			}
		}		
	}
	
	private void swap(int i) {
		Number temp = inputNumbers[i];
		inputNumbers[i] = inputNumbers[i-1];
		inputNumbers[i-1] = temp;
	}

}