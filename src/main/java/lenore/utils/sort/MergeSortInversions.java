package lenore.utils.sort;


public class MergeSortInversions extends MergeSort {
	private long inversions;
	
	public long getInversions() {
		return inversions;
	}

	public void setInversions(long inversions) {
		this.inversions = inversions;
	}

	public MergeSortInversions() {}

	public MergeSortInversions(Number[] input) {
		setInputNumbers(input);
	}

	protected void merge(int low, int middle, int high) {
		// Copy both parts into the helper array
		Number[] helper = new Number[inputNumbers.length];
		System.arraycopy(inputNumbers, 0, helper, 0, inputNumbers.length);
		int i = low;
		int j = middle + 1;
		int k = low;
		// Copy the smallest values from either the left or the right side back
		// to the original array
		while (i <= middle && j <= high) {
			if (helper[i].intValue() < helper[j].intValue()) {
				inputNumbers[k] = helper[i++];
			} else {
				inputNumbers[k] = helper[j++];
				inversions += middle - i + 1;
			}
			k++;
		}
		// Copy the rest of the left side of the array into the target array
		while (i <= middle) {
			inputNumbers[k++] = helper[i++];
		}
		// Copy the rest of the right side of the array into the target array
		while (j <= high)
		    inputNumbers[k++] = helper[j++];
		  
	}

}
