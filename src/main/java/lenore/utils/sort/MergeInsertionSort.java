package lenore.utils.sort;


public class MergeInsertionSort extends MergeSort {
	private int k;
	
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}

	public MergeInsertionSort() { }

	public MergeInsertionSort(Number[] input) {
		super(input);
		this.k = 1;
	}

	public MergeInsertionSort(Number [] input, int k) {
		super(input);
		this.k = k > 0 ? k : 1;
    }
	
	@Override
	protected void sort(int left, int right) {
		if (left < right) {
			// Get the index of the element which is in the middle
			int middle = left + (right - left) / 2;
			//if half is less than k - insertion sort
			if (right - middle <= k) {
				insertionSort(left, middle);
				insertionSort(middle + 1, right);
			} else {
				// Sort the left side of the array
				sort(left, middle);
				// Sort the right side of the array
				sort(middle + 1, right);
			}
			// Combine them both
			merge(left, middle, right);
		}
	}
	
	protected void insertionSort(int left, int right) {
		for (int i = left + 1; i <= right; i++) {
			if (inputNumbers[i].intValue() < inputNumbers[i-1].intValue()) {
				swap(i, left);
			}
		}
	}
	
	public void swap (int index, int left) {
		while (index > left && inputNumbers[index].intValue() < inputNumbers[index-1].intValue()) {
			Number temp = inputNumbers[index];
			inputNumbers[index] = inputNumbers[index-1];
			inputNumbers[index-1] = temp;
			index--;
		}
	}
	
}
