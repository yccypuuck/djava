package lenore.utils.sort;


/**
 * Created by ryzma01 on 5/26/2014. QuickSort
 */
public class QuickSort extends Sort {

	public QuickSort() {}
	public QuickSort(Number[] input) {
		setInputNumbers(input);
	}


	@SuppressWarnings("unchecked")
	protected void sort(int low, int high) {
		// two numbers left, sort them and add one comparison
		if (high - low == 1) {
			if (((Comparable<Number>) inputNumbers[low]).compareTo(inputNumbers[high]) > 0)
				swap(low, high);
			return;
		}
		// one number - nothing to do
		if (high <= low) {
			return;
		}
		// put pivot into the first position
		// swap(low + (high-low)/2,low);
		selectPivotMedian(low, high);
		// get pivot's index
		int index = partition(low, high);
		// recur [] < p > []
		sort(low, index - 1);
		sort(index + 1, high);
	}

	protected int partition(int l, int r) {
		Double pivot = inputNumbers[l].doubleValue();
		// set marker
		int marker = l + 1;
		for (int j = l + 1; j <= r; j++) {
			if ((pivot).compareTo(inputNumbers[j].doubleValue()) > 0) {
				// swap and move marker
				swap(marker++, j);
			}
		}
		// swap pivot value with last value that is less than pivot value
		swap(l, --marker);
		return marker;
	}

	protected void swap(int i, int j) {
		Number buf = inputNumbers[i];
		inputNumbers[i] = inputNumbers[j];
		inputNumbers[j] = buf;
	}

	protected void selectPivotFirst() {
		// nothing to do
	}

	protected void selectPivotLast(int l, int r) {
		swap(l, r);
	}

	protected void selectPivotMedian(int left, int right) {
		int med = ((right - left) / 2) + left;
		double a = (inputNumbers[left]).doubleValue();
		double b = (inputNumbers[med]).doubleValue();
		double c = (inputNumbers[right]).doubleValue();
		int index;

		// a >= b and a <= c OR a <= b and a >= c
		if ((a - b) * (c - a) >= 0)
			index = left;
		// b >= a and b <= c OR b <= a and b >= c
		else if ((b - a) * (c - b) >= 0)
			index = med;
		else
			index = right;
		swap(left, index);
	}

}
