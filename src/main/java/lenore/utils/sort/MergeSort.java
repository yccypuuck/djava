package lenore.utils.sort;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MergeSort extends Sort {

	public final static Logger logger = LogManager.getLogger(MergeSort.class.getName());

	public MergeSort() { }
	public MergeSort(Number[] input) {
		setInputNumbers(input);
	}

	protected void sort(int low, int high) {
		// check if low is smaller then high, if not then the array is sorted
		if (low < high) {
			// Get the index of the element which is in the middle
			int middle = low + (high - low) / 2;
			// Sort the left side of the array
			sort(low, middle);
			// Sort the right side of the array
			sort(middle + 1, high);
			// Combine them both
			merge(low, middle, high);
		}
	}

	@SuppressWarnings("unchecked")
	protected void merge(int low, int middle, int high) {
		// Copy both parts into the helper array
		Number[] helper = new Number[inputNumbers.length];
		System.arraycopy(inputNumbers, 0, helper, 0, inputNumbers.length);
		int i = low;
		int j = middle + 1;
		int k = low;
		// Copy the smallest values from either the left or the right side back
		// to the original array
		while (i <= middle && j <= high) {
			if (((Comparable<Number>) helper[j]).compareTo(helper[i]) > 0) {
				inputNumbers[k] = helper[i];
				i++;
			} else {
				inputNumbers[k] = helper[j];
				j++;
			}
			k++;
		}
		// Copy the rest of the left side of the array into the target array
		while (i <= middle) {
			inputNumbers[k] = helper[i];
			k++;
			i++;
		}
	}

}
