package lenore.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by lenore on 28/03/16.
 * <p>
 * Keeps data for task:
 * - input stream
 * - scanner
 * - reader
 * - original input text
 * <p>
 * Saves output for task
 */
public class ProblemData {

	public final static Logger logger = LogManager.getLogger(ProblemData.class.getName());

	private String input;
	private Scanner scanner;
	protected BufferedReader reader;

	public void setInput(String input) {
		logger.debug(String.format("Input:\n%s", input));
		this.input = input;
	}

	public String getInput() {
		return input;
	}

	public Number[] readArray() {
		if (input == null || input.length() == 0) {
			return null;
		}
		return readArray(input);
	}

	public Number[] readArray(String input) {
		setInput(input);
		parseInputStream();
		return readNumberInputArray();
	}

	private Number[] readNumberInputArray() {
		List<Number> lines = new ArrayList<>();
		while (scanner.hasNext()) {
			Double nextValue = Double.parseDouble(scanner.next());
			lines.add(nextValue);
		}
		return lines.toArray(new Number[lines.size()]);
	}

	private void setInputStream(InputStream input) {
		this.scanner = new Scanner(input);
		this.reader = new BufferedReader(new InputStreamReader(input));
	}

	private InputStream parseInputStream() {
		InputStream stream;
		try {
			stream = inputFromFile(new File(input));
		} catch (FileNotFoundException e) {
			stream = inputFromString(input);
		}
		setInputStream(stream);

		return stream;
	}

	private InputStream inputFromFile(File inputFile) throws FileNotFoundException {
		FileReader fr = new FileReader(inputFile);
		BufferedReader bufferedStream = null;
		InputStream stream = null;
		try {
			bufferedStream = new BufferedReader(fr);
			stream = new FileInputStream(inputFile);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (bufferedStream != null) {
				try {
					bufferedStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return stream;
	}

	private InputStream inputFromString(String input) {
		return new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
	}


	public char[] readCharArray() {
		if (input == null || input.length() == 0) {
			return null;
		}
		return readCharArray(input);
	}

	public char[] readCharArray(String input) {
		setInput(input);
		parseInputStream();
		return readCharInputArray();
	}

	public char[] readCharInputArray() {
		StringBuffer input = new StringBuffer();

		while (scanner.hasNext()) {
			input.append(scanner.next());
		}

		return new String(input).toCharArray();

	}

}
