package lenore.utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProblemRunner {
    public final static Logger logger = LogManager.getLogger(ProblemRunner.class.getName());
    private final Problem problem;

    	public ProblemRunner(Problem problem) {
        this.problem = problem;
    }

    public void run() {
        long start, end;
        start = System.nanoTime();
        problem.run();
        end = System.nanoTime();
        logger.debug(String.format("running time: %d  \tProblem: %s ",(end - start), problem.getClass().getName()));
    }

    public void noDbgMode() {
    	logger.isEnabled(Level.WARN);
    }

    public Problem getProblem() {
		return problem;
	}

    public boolean verifyOutput(String output) {
    	return problem.verifyOutput(output);
    }

    public void inputFromFile(String inputFile) {
		problem.inputFromFile(inputFile);
	}

	public void inputFromString(String inputString) {
		problem.inputFromString(inputString);
	}

}