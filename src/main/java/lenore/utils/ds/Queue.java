package lenore.utils.ds;

import lenore.utils.ds.node.NodeImpl;


public class Queue  {
	private NodeImpl first, last;
	
	void enQueue(int d){
		if (first == null) {
			last = new NodeImpl(d);
			first = last;
		}
		else {
			last.next = new NodeImpl(d);
			last = last.next;			
		}
	}
	
	int deQueue() {
		if (first != null) {
			int d = first.data;
			first = first.next;
			return d;			
		}
		return 0;
	}
	
	boolean isEmpty() {
		return (first == null);
	}
}
