package lenore.utils.ds.queue;

import lenore.utils.ds.node.NodeImpl;

public class Queue {
	NodeImpl first, last;
	
	void enqueue(int d){
		if (first == null) {
			last = new NodeImpl(d);
			first = last;
		}
		else {
			last.next = new NodeImpl(d);
			last = last.next;			
		}
	}
	
	int dequeue() {
		if (first != null) {
			int d = first.data;
			first = first.next;
			return d;			
		}
		return 0;
	}
}
