package lenore.utils.ds;

import lenore.utils.ds.node.NodeImpl;

import java.util.ArrayList;
import java.util.List;

public class LinkList {
    private NodeImpl first;

    //LinkList constructor
    public LinkList() {
	    first = null;
    }

    //Returns true if list is empty
    public boolean isEmpty() {
	    return first == null;
    }

    //Inserts a new Link at the first of the list
    public void insert(int d) {
	    NodeImpl link = new NodeImpl(d);
	    link.next = first;
	    first = link;
    }
    
    //Inserts to the end of the list
    public void append(int d) {
    	NodeImpl link = first;
    	if (this.isEmpty()) {
    		this.insert(d);
    		return;
    	}
    	while(link.next != null) {
    		link = link.next;
    	}
    	link.next = new NodeImpl(d);
    }

    //Deletes the link at the first of the list
    public NodeImpl delete() {
	    NodeImpl temp = first;
	    first = first.next;
	    return temp;
    }
    
    //Deletes a node with a data
    public void deleteNodeByData(int d) {
        NodeImpl n = first;
        if (n.data == d) {
            this.delete();   // moved head 
        }
        while (n.next != null) {
            if (n.next.data == d) {
                n.next = n.next.next;
                return; // head didn't change 
            }
            n = n.next;
        }
    }
    
    //Generate a list with n elements with a data 0 .. n
    public void generateList(int n) {
        for (int i = 0; i < n; i++) {
            this.append(i);
        }   
    }
    
  //Generate a list with n elements with a data increasing by step
    public void generateList(int n, double step) {
    	double key = 0;
        for (int i = 0; i < n; i++,key += step) {
            this.append((int)key);
        }   
    }

    //Prints whole list data
    public void printList() {
	    NodeImpl currentLink = first;
	    System.out.print("List: ");
    	if (this.isEmpty()) System.out.print("is EMPTY!");
	    while(currentLink != null) {
            System.out.print(currentLink);
		    currentLink = currentLink.next;
	    }
	    System.out.println("");
    }
    
    //Prints whole list with a name
    public void printList(String lName) {
	    NodeImpl currentLink = first;
	    System.out.print(lName + ": ");
    	if (this.isEmpty()) System.out.print("is EMPTY!");
	    while(currentLink != null) {
            System.out.print(currentLink);
            currentLink = currentLink.next;
	    }
	    System.out.println("");
    }
    
    //Deletes node with specifying the pointer to it
    public void deleteNode(NodeImpl n){
        if (n.next == null) return;
        n.data = n.next.data;
        n.next = n.next.next;
    }
    
    
    //Delete duplicates from the list using additional storage
    public void deleteDupStorage () {
    	if (this.isEmpty()) return;
        List <Integer> values = new ArrayList<>();
        NodeImpl prev = null;
        NodeImpl runner = first;
        while (runner != null) {
            if (prev != null && values.contains(runner.data)) {
                prev.next = runner.next;
            }
            else {
                values.add(runner.data);
                prev = runner;
            }
            runner = runner.next;
        }
    }
    
    //Delete duplicates without using any storage
    public void deleteDup() {
    	if (this.isEmpty()) return;
    	NodeImpl runner = first;
        while (runner.next != null){
            NodeImpl n = runner.next;
            while (n != null) {
            	while (runner.data == n.data) {
            		this.deleteNode(n);
            	}
                n = n.next;
            }
            runner = runner.next;
        }
    }    
    
    //Find the node n-th from the last
    public NodeImpl findLastNthNode(int n) {
        NodeImpl head = first;
        NodeImpl tail = first;
        int i = 0;
        while(i++ < n) {
            tail = tail.next;
            if (tail.next == null) return null;
        }
        while (tail.next != null) {
            head = head.next;
            tail = tail.next;
        }
        return head;
    }
    
    //Linked list sum
    public LinkList addList (LinkList list) {
    	int carrier = 0;
    	NodeImpl ll1 = this.first;
    	NodeImpl ll2 = list.first;
    	LinkList result = new LinkList();
    	while((ll1 != null)||(ll2 != null) || (carrier != 0)){
    		int el1 = 0, el2 = 0;
    		if (ll1 != null) el1 = ll1.data;
    		if (ll2 != null) el2 = ll2.data;
    		int el3 = (el1 + el2 + carrier) % 10;
    		carrier = (el1 + el2 + carrier) / 10;
    		result.append(el3);
    		if (ll1 != null) ll1 = ll1.next;
    		if (ll2 != null) ll2 = ll2.next;
    	}
    	return result;
    }
    
    //Corrupt the list - make an circular linked list at any node
    public void corruptList(int n) {
    	NodeImpl tail = first;
    	NodeImpl corruptedNode = first;
    	while(tail.next != null) {
    		tail = tail.next;
    	}
    	while(corruptedNode != null) {
    		if (n == 0) {
                System.out.print(corruptedNode);
    			tail.next = corruptedNode;
    			return;
    		}
    		n--;
    		corruptedNode = corruptedNode.next;
    	}
    }
    
    //Finds circular corrupted node with additional storage
    public NodeImpl findCircularStorage() {
    	List <NodeImpl> values = new ArrayList <>();
    	NodeImpl runner = first;
    	while (runner != null) {
    		if (!values.contains(runner)) {
    			values.add(runner);
    			runner = runner.next;
    		}
    		else return runner;
    	}
    	return null;
    }
    
    //Finds corrupted node without additional storage
    public NodeImpl findCircular() {
    	NodeImpl tail = first;
    	while(tail != null) {
    		NodeImpl runner = first;
    		while(runner != tail) {
	    		if ((tail == tail.next)||(runner == tail.next)) { 
	    			return tail.next;
	    		}
	    		runner = runner.next;
    		} 
    		tail = tail.next;
    	}
    	return tail;
    }
}  