package lenore.utils.ds.node;


public class NodeStack implements Node {
    public int data;
    private int min;

    public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

    public NodeStack(int d) {
	    data = d;
    }

    @Override
    public String toString() {
        return ("[" + data + "] ");
    }

}
