package lenore.utils.ds.node;

public class NodeImpl implements Node{
	public int data;
	public NodeImpl next;

	public NodeImpl(int x) {
		data = x;
		next = null;
	}
	
	public NodeImpl(int[] array) {
		data = array[0];
		NodeImpl node = new NodeImpl(0);
		next = node;
		for (int i = 1; i < array.length; i++) {
			node.data = array[i];
			if (i < array.length - 1) {
				node.next = new NodeImpl(0);
			}
			node = node.next;
		}
	}

	public void printList () {
		NodeImpl sum = this;
		while (sum != null) {
			System.out.print(sum.data + " ");
			sum = sum.next;
		}
		System.out.println();
	}
	
	@Override
	public String toString() {
		return ("[" + data + "] ");
	}
}
