package lenore.utils.ds.tree;

import java.util.Random;

/**
 * Created by ryzma01 on 5/28/2014.
 *
 * Generator of a random binary tree.
 * uses:
 * RndTree newTree = new RndTree(20); - will generate tree with depth 20
 * RndTree newTree = new RndTree(); - defaul depth is 5
 */
public class RndTree extends BinaryTree {
    private TreeNode root;
    Random randomGenerator = new Random();
    Random nullGenerator = new Random();

    public RndTree() {
        root = buildTree(5);
    }

    public RndTree(int maxLevel) {
        if (maxLevel == 0) return;
        root = buildTree(maxLevel);
    }

    private TreeNode getRndNode() {
        if (nullGenerator.nextInt(10) > 7) {
            return null;
        }
        int randomInt = randomGenerator.nextInt(100);
        return new TreeNode(randomInt);
    }

    public TreeNode buildTree (int maxLevel) {
        root = addNode(root, maxLevel);
        return root;
    }

    private TreeNode addNode(TreeNode pointer, int maxLevel) {
        if (maxLevel == 0) return null;
        pointer = getRndNode();
        if (pointer == null) return null;
        pointer.left = addNode(pointer.left, (maxLevel - 1));
        pointer.right = addNode(pointer.right, (maxLevel - 1));
        return pointer;
    }
}
