package lenore.utils.ds.tree;

public class TreeNode {
	public int children;
    public int value;
    public TreeNode left;
    public TreeNode right;

    //Link constructor
    public TreeNode(int d) {
	    value = d;
	    children = 0;
    }

    //Print Link data
    public String toString() {
	    return ("[" + value + "] ");
    }

}
