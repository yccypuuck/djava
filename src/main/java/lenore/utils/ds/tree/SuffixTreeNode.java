package lenore.utils.ds.tree;

import java.util.Hashtable;

/**
 * Created by lenore on 9/14/14.
 */
public class SuffixTreeNode {

    private String value;
    private int index;
    private Hashtable<String,SuffixTreeNode> nodes;

    public SuffixTreeNode() {

    }

    private boolean ending = true;
    public boolean isEnding() {
        return ending;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void addToNodes(String value) {
        SuffixTreeNode node = new SuffixTreeNode();
        node.setValue(value);
        nodes.put(value, node);
    }
}
