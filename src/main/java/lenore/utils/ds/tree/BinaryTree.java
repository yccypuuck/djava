package lenore.utils.ds.tree;

import java.util.Random;

public class BinaryTree {
	protected TreeNode root; // root of the tree

	public BinaryTree() {
		root = null;
	}

	// adds a value to a root
	public void add(int d) {
		if (isEmpty()) {
			root = new TreeNode(d);
			return;
		}
		add(root, d);
	}
	
	public TreeNode getRoot() {
		return this.root;
	}

	// adds a value starting from some particular node. may screw the system
	public void add(TreeNode node, int d) {
		// if (d == node.data) return;

		if (node.left == null) {
			node.left = new TreeNode(d);
			return;
		}
		if (node.right == null) {
			node.right = new TreeNode(d);
			return;
		}
		if (node.left.children <= node.right.children) {
			add(node.left, d);
		} else {
			add(node.right, d);
		}
		node.children++;
	}

	public boolean isEmpty() {
		return (root == null);
	}

	public TreeNode getTree() {
		return root;
	}

	/*---------------------------------------------------------------------*\
	 *                                                                       *
	 *                       T R A V E R S A L S                             *
	 *                                                                       *
	\*---------------------------------------------------------------------*/

	// Print the items in the tree in post-order,
	// Traverse the left subtree. -> Traverse the right subtree. -> Visit the
	// root.
	public void postOrder() {
		System.out.print("Post-order traversal: ");
		postOrder(root);
		System.out.println();
	}

	public void postOrder(TreeNode node) {
		if (node == null)
			return;
		postOrder(node.left);
		postOrder(node.right);
		System.out.print(node);
	}

	// Print the items in the tree in pre-order,
	// Visit the root. -> Traverse the left subtree. -> Traverse the right
	// subtree.
	public void preOrder() {
		System.out.print("Pre-order traversal:  ");
		preOrder(root);
		System.out.println();
	}

	public void preOrder(TreeNode node) {
		if (node == null)
			return;
		System.out.print(node);
		preOrder(node.left);
		preOrder(node.right);
	}

	// Print the items in the tree from left to right,
	// Traverse the left subtree. -> Visit the root. -> Traverse the right
	// subtree.
	public void inOrder() {
		System.out.print("In-order traversal:   ");
		inOrder(root);
		System.out.println();
	}

	public void inOrder(TreeNode node) {
		if (node == null)
			return;
		inOrder(node.left);
		System.out.print(node);
		inOrder(node.right);
	}

	public void generateTree (int nodes) {
		Random rand  = new Random();
		rand.setSeed(System.currentTimeMillis());
		for (int i = 0; i < nodes; i++) {
			this.add(rand.nextInt(nodes*10));
		}
	}
}
