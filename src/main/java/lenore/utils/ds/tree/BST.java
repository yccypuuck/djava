package lenore.utils.ds.tree;

public class BST extends BinaryTree {

	public void add(int d) {
		if (this.root == null) {
			this.root = new TreeNode(d);
			return;
		}

		add(root, d);
	}

	public void add(TreeNode node, int d) {
		if (node == null) {
			node = new TreeNode(d);
		}
		if (d == node.value)
			return;

		if (d < node.value) {
			if (node.left == null) {
				node.left = new TreeNode(d);
			}
			add(node.left, d);
			return;
		}

		if (node.right == null) {
			node.right = new TreeNode(d);
		}
		add(node.right, d);
	}

	public TreeNode search(int d) {
		if (root == null)
			return null;
		return search(root, d);
	}

	private TreeNode search(TreeNode node, int d) {
		if (node == null)
			return null;
		if (node.value == d)
			return node;
		if (d <= node.value) {
			return search(node.left, d);
		} else {
			return search(node.right, d);
		}
	}


	/**
	 * Test cases:
	 * 1 : values in left sub-tree are less than current node && values in right sub-tree are greater than current node 
	 * 2 : null tree
	 * 3 : duplicate values 
	 * 4 : broken binary tree (loops, two nodes have same child) 
	 * 5 : Integer min & max as valid numbers
	 * 
	 */

	public boolean isValid() {
		if (this.root == null) {
			return true;
		}
		// check both subtrees in ranges:
		// [integer.min;root) for left subtree
		// (root;integer.max] for right subtree
		return isValidSubTree(this.root.left, Integer.MIN_VALUE,
				this.root.value)
				&& isValidSubTree(this.root.right, this.root.value,
						Integer.MAX_VALUE);
	}

	public boolean isValid(TreeNode root) {
		if (root == null) {
			return true;
		}
		// both subtrees should be valid
		return isValidSubTree(root.left, Integer.MIN_VALUE, root.value)
				&& isValidSubTree(root.right, root.value, Integer.MAX_VALUE);
	}

	private boolean isValidSubTree(TreeNode root, int min, int max) {
		// valid if root is null
		if (root == null) {
			return true;
		}

		// root.value has to be in range:
		// 1. (min;max)
		// 2. [integer.min_value;max) or (min;integer.max_value]
		// define simple left & right from complicated situation
		int left = (min == Integer.MIN_VALUE) ? Integer.MIN_VALUE : min + 1;
		int right = (max == Integer.MAX_VALUE) ? Integer.MAX_VALUE : max - 1;

		// invalid if root is outside boundaries
		if (root.value < left || root.value > right) {
			return false;
		}
		// recursive processing
		return isValidSubTree(root.left, min, root.value)
				&& isValidSubTree(root.right, root.value, max);
	}

}
