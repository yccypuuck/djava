package lenore.utils.ds;

import java.util.Stack;

import lenore.utils.ds.node.NodeStack;

/**
 * 
 * Design a stack which, in addition to push and pop, also has a function min
 * which returns the minimum element. Push, pop and min should all operate in
 * O(1) time
 * 
 * @author lenore
 */

public class StackMin extends Stack<NodeStack> {

	public int push(int d) {
		NodeStack tempNode = new NodeStack(d);
		if (this.size() != 0) {
			int minOld = peek().getMin();

			if (d < minOld) {
				tempNode.setMin(d);
			} else {
				tempNode.setMin(minOld);
			}
		} else {
			tempNode.setMin(d);
		}
		super.push(tempNode);
		return d;
	}

	public int getMinimum() {
		return peek().getMin();
	}

	private static final long serialVersionUID = 192347658L;
}
