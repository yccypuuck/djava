package lenore.utils.ds;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by ryzma01 on 5/30/2014.
 * Graph parsing from file in Adjacency list style
 */
public class AdjacencyListGraph {
    protected Map<Number, List<Number>> graph;
    protected List<Number> allVerticesList;

    public AdjacencyListGraph() {
        graph = new HashMap<Number, List<Number>>();
        allVerticesList = new ArrayList<Number>();
    }

    public void readGraph(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            addVertices(line);
        }
        bufferedReader.close();
    }

    public void addVertices(String line) {
        List<String> s = new ArrayList<String>(Arrays.asList((line.split("\\s+"))));
        List<Number> edge = new ArrayList<Number>();
        Integer vertex = Integer.parseInt(s.get(0));
        for (int i = 1; i < s.size(); i++) {
            edge.add(Integer.parseInt(s.get(i)));
        }
        allVerticesList.add(vertex);
        graph.put(vertex, edge);
    }

    public void printGraph() {
        for (Number i : allVerticesList) {
            System.out.println(i + ": " + graph.get(i));
        }
    }
    public int size() {
        return allVerticesList.size();
    }
}
