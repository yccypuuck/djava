package lenore.utils.ds.heap;

import java.util.Comparator;

public class MinHeap<T> extends Heap<T> {

	public MinHeap(Comparator<T> comparator) {
		super(comparator);
	}

	public boolean add(T element) {
		heap.add(element);
		int current = size++;
		while (comparator.compare(heap.get(current), heap.get(parent(current))) == 1) {
			swap(current, parent(current));
			current = parent(current);
		}
		return true;
	}

	void heapify(int i) {
		int smallest = i;
		int lc = (i + 1) * 2 - 1;
		int rc = (i + 1) * 2;
		if (lc <= size && comparator.compare(heap.get(lc), heap.get(i)) == 1)
			smallest = lc;
		if (rc <= size && comparator.compare(heap.get(rc), heap.get(smallest)) == 1)
			smallest = rc;
		if (smallest != i) {
			swap(smallest, i);
			heapify(smallest);
		}
	}

}
