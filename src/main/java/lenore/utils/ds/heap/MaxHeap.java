package lenore.utils.ds.heap;

import java.util.Comparator;
import java.util.Random;

public class MaxHeap<T> extends Heap<T> {

	public MaxHeap(Comparator<T> comparator) {
		super(comparator);
	}

	public boolean add(T element) {
		heap.add(element);
		int current = size++;
		while (comparator.compare(heap.get(current), heap.get(parent(current))) == -1) {
			swap(current, parent(current));
			current = parent(current);
		}
		return true;
	}

	void heapify(int i) {
		int largest = i;
		int lc = (i + 1) * 2 - 1;
		int rc = (i + 1) * 2;
		if (lc <= size && comparator.compare(heap.get(lc), heap.get(i)) == -1)
			largest = lc;
		if (rc <= size && comparator.compare(heap.get(rc), heap.get(largest)) == -1)
			largest = rc;
		if (largest != i) {
			swap(largest, i);
			heapify(largest);
		}
	}

	public static void main(String[] args) {
		Random rnd = new Random();
		Comparator<Integer> comparator = new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		};
		MaxHeap<Integer> heap = new MaxHeap<Integer>(comparator);
		for (int i = 0; i < 20; i++) {
			int randInt = rnd.nextInt(200);
			System.out.println(randInt);
			heap.add(randInt);
			System.out.println(heap.toString());
		}

		System.out.println(heap.toString());

		while (!heap.isEmpty()) {
			System.out.println(heap.pop());
			System.out.println(heap.toString());
		}
	}
}
