package lenore.utils.ds.heap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class Heap<T> {
	List<T> heap;
	Comparator<T> comparator;
	int size;
	
	
	public Heap(Comparator<T> comparator) {
		size = 0;
		heap = new ArrayList<T>();
		this.comparator = comparator;
	}


	// Adds an object to the heap.
	public abstract boolean add(T element);

	// Clears all elements from queue.
	public void clear() {
		heap.clear();
		size = 0;
	}

	// Tests if queue is empty.
	public boolean isEmpty() {
		return size == 0;
	}

	// Returns the element on top of heap and removes it.
	public T pop() {
		if (isEmpty())
			throw new IndexOutOfBoundsException("Your heap is empty!");
		T head = heap.get(0);
		heap.set(0, heap.get(--size));
		heapify(0);
		return head;
	}

	abstract void heapify(int i);

	// Returns the number of elements in this heap.
	public int size() {
		return size;
	}

	// Returns a string representation of this heap.
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.size; i++) {
			sb.append(heap.get(i)).append(" ");
		}
		return "heap: " + sb.toString();
	}

	// Returns the element on top of heap but doesn't remove it.
	public T peek() {
		if (isEmpty())
			throw new IndexOutOfBoundsException("Your heap is empty!");
		return heap.get(0);
	}

	// Inserts an element into queue.
	public void insert(T element) {
		this.add(element);
	}

	// Returns the priority element.
	public T get() {
		return peek();
	}

	// Removes the priority element.
	public T remove() {
		return pop();
	}

	void swap(int current, int parent) {
		T swap = heap.get(current);
		heap.set(current, heap.get(parent));
		heap.set(parent, swap);
	}

	int parent(int current) {
		return (current - 1) / 2;
	}

}
