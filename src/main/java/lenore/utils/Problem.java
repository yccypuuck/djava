package lenore.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by ryzma01 on 6/27/2014.
 * Initial state for each task.
 */

public abstract class Problem extends ProblemData implements Runnable {
	public final static Logger logger = LogManager.getLogger(Problem.class.getName());

	private String input;

	protected Number[] readInputArray() {
		return readArray();
	}

	public void inputFromString(String input) {
		setInput(input);
	}

	public void inputFromFile(String name) {
		setInput(name);
	}

	public boolean verifyOutput(String output) {
		//TODO
		return input.equals(output);
	}
}