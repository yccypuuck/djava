package lenore.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class TestRunner {
	List<ProblemRunner> tasks = new ArrayList<>();
	public final static Logger logger = LogManager.getLogger(TestRunner.class
			.getName());
	private String inputFile = null;
	private String inputString = null;

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public void setTasks(List<ProblemRunner> tasks) {
		this.tasks = tasks;
	}

	public void run() {
		for (ProblemRunner task : tasks) {
			if (task.getProblem().getInput() == null) {
				if (inputFile != null) {
					task.inputFromFile(inputFile);
				} else if (inputString != null) {
					task.inputFromString(inputString);
				}
			}
			task.run();
		}
	}

	public void add(Problem problem) {
		tasks.add(new ProblemRunner(problem));
	}

}
