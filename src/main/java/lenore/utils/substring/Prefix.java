package lenore.utils.substring;

/**
 * Created by lenore on 9/13/14.
 */
public class Prefix {

    private char[] s;

    public char[] getS() {
        return s;
    }

    public void setS(char[] s) {
        this.s = s;
    }

    public Prefix (char[] s) {
        this.s = s;
    }

    public int[] findPrefix () {
        if (s.length == 0){
            return null;
        }

        return findPrefix(0);
    }

    public int[] findPrefix(int start) {
        if (start >= s.length){
            return null;
        }

        int [] v = new int[s.length];

        for (int i = start+1; i < s.length; i++) {
            int k = v[i-1];
            while (k > 0 && s[k] != s[i]) {
                k = v[k-1];
            }
            if (s[k] == s[i]) {
                k = k + 1;
            }
            v[i] = k;
        }

        return v;
    }

}
